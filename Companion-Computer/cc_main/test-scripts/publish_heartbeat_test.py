#!/usr/bin/env python

import sys
sys.path.append("..")

import time
import paho.mqtt.client as mqtt
# import cc_fsm.protocol.cc_messages_pb2 as messages
from cc_fsm.protocol import cc_messages_pb2 as messages


def main():
    mqtt_client = mqtt.Client()
    mqtt_client.connect("localhost", 1883)

    # start background thread for network stuff
    mqtt_client.loop_start()

    time.sleep(0.5)

    msg = messages.heartbeat()
    msg.type = 6
    msg.autopilot = 8
    msg.base_mode = 136
    msg.custom_mode = 0
    msg.system_status = 4
    msg.system_id = 255

    payload = bytearray(msg.SerializeToString())

    # make enough heartbeats to test changing state
    for i in range(1, 10):
        mqtt_client.publish("heartbeat_rx", payload)
        print("heartbeat sent")
        time.sleep(0.2)

    print("exiting program")

    mqtt_client.loop_stop()
    mqtt_client.disconnect()


if __name__ == '__main__':
    main()
