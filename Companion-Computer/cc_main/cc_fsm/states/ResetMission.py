#!/usr/bin/env python

import threading
import time

import smach
from cc_fsm.CustomDefines import *
import cc_fsm.protocol.cc_messages_pb2 as messages


# define state
class ResetMission(smach.State):
    def __init__(self):
        # init smach state
        smach.State.__init__(self,
                             outcomes=['telemetry_control'],
                             input_keys=['missionList', 'mqtt_client'],
                             output_keys=['missionList', 'mqtt_client'])

        # vars
        self.ud = None
        self.client = None

        # workaround to change state from callback functions
        self.nextState = ""
        self.mutex = threading.Lock()

        # setup subs


    def on_state_transition(self):
        self.mutex.release()
        # release mqtt callback?
        self.client.message_callback_remove("heartbeat_rx")

    def ack_callback(self, msg):
        # if accepted, clear CCs mission list and go back to telemetry state
        if msg == "MAV_MISSION_ACCEPTED":
            self.mutex.acquire()
            self.nextState = "telemetry_control"
            self.ud.missionList = []
            self.mutex.release()

        # todo else handle rejections...

    def execute(self, userdata):
        print("Executing State RESET_MISSION")

        # todo clear mission list to px4 through mavlink_lora. Try multiple times if it fails.
        #  can be extended to reacting on how it fails.

        # todo when mission is cleared and local saved mission_list is cleared, go to telemetry_control
        #  variable that tells telemetry_control to ask for new path? Ideally GCS should know by itself as
        #  communication was lost

        # save userdata so class can use it?
        self.ud = userdata
        self.client = self.ud.mqtt_client

        # send mission clear
        # todo make it repeat if failed, here we assume it works as expected every time
        #  mavlink lora already retries the command so might be fine to assume we only need to retry if px4 reject it.

        # register mqtt callback
        # self.client.message_callback_add("heartbeat_rx", self.heartbeat_callback)

        while True:
            # check if we should change state
            self.mutex.acquire()

            if self.nextState != "":
                self.on_state_transition()
                return self.nextState
            self.mutex.release()

            # sleep for 100ms
            time.sleep(0.1)

