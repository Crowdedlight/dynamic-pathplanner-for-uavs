#!/usr/bin/env python

import rospy
import smach
import threading
from ..CustomDefines import *
from mavlink_lora.msg import mavlink_lora_heartbeat


# define state
class Boot(smach.State):

    # INIT
    def __init__(self):
        # init smach state
        smach.State.__init__(self,
                             outcomes=['idle'],
                             input_keys=['telemStatus', 'SOC', 'flightmode', 'armed'],
                             output_keys=['telemStatus', 'SOC', 'flightmode', 'armed'])

        # Vars
        self.heartbeat_last_received = 0
        self.last_heartbeats_periods = []
        self.ud = None
        self.hb_sub = None

        # workaround to change state from callback functions
        self.nextState = ""
        self.mutex = threading.Lock()
        self.rate = rospy.Rate(10)

    def execute(self, userdata):
        rospy.loginfo("Executing State BOOT")

        # save userdata so class can use it
        self.ud = userdata

        # clean nextstate variable if we are revisiting the state
        self.nextState = ""

        # setup subs
        self.hb_sub = rospy.Subscriber("mavlink_heartbeat_rx", mavlink_lora_heartbeat, self.heartbeat_callback)

        while not rospy.is_shutdown():

            # If stuff is initalized and telemetry connected change state
            self.mutex.acquire()

            if self.ud.telemStatus == TelemetryStatus.connected:
                self.on_state_transition()
                return "idle"

            self.mutex.release()

            self.rate.sleep()

    def on_state_transition(self):
        # unregister all subs and release all mutexes
        self.hb_sub.unregister()
        self.mutex.release()

    # CALLBACKS
    def heartbeat_callback(self, msg):
        # mark time now
        now = rospy.get_time()

        rospy.loginfo("Heartbeat received")

        # if first time, mark now and exit
        if self.heartbeat_last_received == 0:
            self.heartbeat_last_received = now
            return

        # calculate period since last heartbeat
        period = now - self.heartbeat_last_received

        # add to heartbeat list
        self.last_heartbeats_periods.append(period)

        # remove first from array if more than 4
        if len(self.last_heartbeats_periods) > 4:
            self.last_heartbeats_periods.pop(0)

        # if length is 4 calculate average period
        heartbeat_avg_period = 5000
        if len(self.last_heartbeats_periods) >= 3:
            heartbeat_avg_period = sum(self.last_heartbeats_periods) / len(self.last_heartbeats_periods)
            # debug
            rospy.loginfo("len: {}, avg period: {}".format(len(self.last_heartbeats_periods), heartbeat_avg_period))


        self.ud.armed = decode_armed(msg.base_mode)
        self.ud.flightmode = decode_custom_flightmode(msg.custom_mode)

        # last received heartbeat
        self.heartbeat_last_received = rospy.get_time()

        # mutex as we access shared status
        self.mutex.acquire()

        # if more than 3 heartbeats have been received and time between them is less than 2sec move on
        if heartbeat_avg_period <= 2 and self.ud.telemStatus != TelemetryStatus.connected:
            # set status as connected and change state
            self.ud.telemStatus = TelemetryStatus.connected

        self.mutex.release()

