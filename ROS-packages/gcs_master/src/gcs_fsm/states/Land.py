#!/usr/bin/env python

import rospy
import smach
import threading
from ..CustomDefines import *
from mavlink_lora.msg import mavlink_lora_heartbeat


# define state
class Land(smach.State):

    # INIT
    def __init__(self):
        # init smach state
        smach.State.__init__(self,
                             outcomes=['post_flight'],
                             input_keys=['telemStatus', 'armed', 'flightmode'],
                             output_keys=['telemStatus', 'armed', 'flightmode'])

        # Vars
        self.ud = None

        # workaround to change state from callback functions
        self.nextState = ""
        self.mutex = threading.Lock()
        self.rate = rospy.Rate(10)
        self.hb_sub = None

    def execute(self, userdata):
        rospy.loginfo("Executing State Land")

        # save userdata so class can use it
        self.ud = userdata

        # clean nextstate variable if we are revisiting the state
        self.nextState = ""

        # setup subs
        self.hb_sub = rospy.Subscriber("mavlink_heartbeat_rx", mavlink_lora_heartbeat, self.heartbeat_callback)

        while not rospy.is_shutdown():

            # If stuff is initalized and telemetry connected change state
            self.mutex.acquire()

            # check if we are landed and disarmed
            if not self.ud.armed:
                rospy.loginfo("Drone disarmed")
                self.on_state_transition()
                self.nextState = "post_flight"

            if self.nextState != '':
                return self.nextState

            self.mutex.release()

            self.rate.sleep()

    def on_state_transition(self):
        # unregister all subs and release all mutexes
        self.hb_sub.unregister()
        self.mutex.release()

    # CALLBACKS
    def heartbeat_callback(self, msg):
        # check for disarmed state
        self.mutex.acquire()

        self.ud.armed = decode_armed(msg.base_mode)
        self.ud.flightmode = decode_custom_flightmode(msg.custom_mode)

        self.mutex.release()


# TODO this state should pretty much do nothing but let the drone land.
#  Just monitor when drone is disarmed as it is landed, and then move to post-flight-check.
#  Idea behind state is to ensure that when drone is landing it can't be redirected => On SHORT FINAL

