#!/usr/bin/env python

from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

setup_args = generate_distutils_setup(
     packages=['utm_link'],
     package_dir={'': 'src'},
     install_requires=['numpy', 'enum34']
)

setup(**setup_args)