#!/usr/bin/env python

import rospy
import smach
import threading
from ..CustomDefines import *
from ..HeartbeatMonitor import HeartbeatMonitor
from mavlink_lora.msg import mavlink_lora_command_ack, mavlink_lora_command_start_mission, mavlink_lora_command_set_mode, mavlink_lora_heartbeat


# define state
class Takeoff(smach.State):

    # INIT
    def __init__(self):
        # init smach state
        smach.State.__init__(self,
                             outcomes=['flight'],
                             input_keys=['telemStatus', 'flightmode', 'armed'],
                             output_keys=['telemStatus', 'flightmode', 'armed'])

        # Vars
        self.ud = None
        self.flightmode_sent = False
        self.start_mission_sent = False
        self.flightmode_success = False
        self.rate = rospy.Rate(10)
        self.resend_timer = None

        # workaround to change state from callback functions
        self.nextState = ""
        self.mutex = threading.Lock()

        # setup heartbeat monitoring
        # self.monitorThread = HeartbeatMonitor(self.heartbeat_lost)
        # self.monitorThread.setName("Heartbeat Monitor")

        # setup subs
        self.hb_sub = None
        self.cmd_ack_sub = None

        # setup pubs
        self.startMission_pub = rospy.Publisher('mavlink_interface/command/start_mission', mavlink_lora_command_start_mission, queue_size=1)
        self.setMode_pub = rospy.Publisher('mavlink_interface/command/set_mode', mavlink_lora_command_set_mode, queue_size=1)

    def execute(self, userdata):
        rospy.loginfo("Executing State Takeoff")

        # save userdata so class can use it
        self.ud = userdata

        # clean nextstate variable if we are revisiting the state
        self.nextState = ""

        # setup subs
        self.cmd_ack_sub = rospy.Subscriber("mavlink_interface/command/ack", mavlink_lora_command_ack, self.command_ack)
        self.hb_sub = rospy.Subscriber("mavlink_heartbeat_rx", mavlink_lora_heartbeat, self.heartbeat_callback)

        while not rospy.is_shutdown():
            self.mutex.acquire()

            # set flightmode to mission
            if self.ud.telemStatus != TelemetryStatus.disconnected and not self.flightmode_sent:
                self.send_flightmode_mission()

            # TODO should arm itself and takeoff unless there is issues.
            #  Should actually also change flightmode, but we do it manually for now.
            # send start_mission command
            if self.ud.telemStatus != TelemetryStatus.disconnected and not self.start_mission_sent and self.flightmode_success:
                self.send_start_mission()

            # change state if new state is set
            if self.nextState != '':
                self.on_state_transition()
                return self.nextState

            self.mutex.release()

            self.rate.sleep()

    def on_state_transition(self):
        # unregister all subs and release all mutexes
        self.hb_sub.unregister()
        self.cmd_ack_sub.unregister()
        self.mutex.release()

    def send_flightmode_mission(self):
        rospy.loginfo("Sending setmode: MISSION")
        # don't resend until we got answer
        self.flightmode_sent = True

        # make command and send
        msg = mavlink_lora_command_set_mode()

        msg.mode = BaseMode.MAV_MODE_FLAG_CUSTOM_MODE_ENABLED
        msg.custom_mode = PX4_CustomMainMode.PX4_CUSTOM_MAIN_MODE_AUTO
        msg.custom_sub_mode = PX4_CustomSubMode.PX4_CUSTOM_SUB_MODE_AUTO_MISSION

        self.setMode_pub.publish(msg)

    def send_start_mission(self):
        rospy.loginfo("Sending command: START_MISSION")
        # don't resend until we got answer
        self.start_mission_sent = True

        # make command and send
        msg = mavlink_lora_command_start_mission()
        msg.first_item = 0  # only first_item is used in px4 code
        self.startMission_pub.publish(msg)

    # CALLBACKS
    def command_ack(self, msg):
        self.mutex.acquire()

        # debug
        rospy.loginfo("Command: " + str(msg.command) + ", result: " + str(msg.result_text))

        # if command is rejected, wait 2sec and try to send again
        if msg.result != CommandResult.MAV_RESULT_ACCEPTED:
            # check what command it was and resend it
            if msg.command == MAVLINK_CMD_SET_MODE_ID:
                # send again in 2sec
                rospy.loginfo("Retrying in 2 seconds")
                self.resend_timer = threading.Timer(2.0, self.send_flightmode_mission)
                self.resend_timer.start()

            elif msg.command == MAVLINK_CMD_MISSION_START_ID:
                # send again in 2sec
                rospy.loginfo("Retrying in 2 seconds")
                self.resend_timer = threading.Timer(2.0, self.send_start_mission)
                self.resend_timer.start()

        # else if mission accepted, move to next command or state
        elif msg.result == CommandResult.MAV_RESULT_ACCEPTED:
            # if setmode, send start mission
            if msg.command == MAVLINK_CMD_SET_MODE_ID:
                self.flightmode_success = True

            # if start mission, we are now in flight state
            if msg.command == MAVLINK_CMD_MISSION_START_ID:
                self.nextState = "flight"

        self.mutex.release()

    def heartbeat_callback(self, msg):
        self.mutex.acquire()

        # check armed and flightmode state
        self.ud.armed = decode_armed(msg.base_mode)
        self.ud.flightmode = decode_custom_flightmode(msg.custom_mode)

        self.mutex.release()

    def heartbeat_lost(self, status):
        # set telem as lost
        self.mutex.acquire()

        # set new status, should update for both disconnected and connected
        self.ud.telemStatus = status

        self.mutex.release()

