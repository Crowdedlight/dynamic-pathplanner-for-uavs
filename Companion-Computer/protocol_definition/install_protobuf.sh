#!/bin/sh
#
# Installs all required dependencies. Taken from github issue: https://github.com/eclipse/paho.mqtt.cpp/issues/136#issuecomment-355280926
#

PROJECT_DIR="$( cd "$( dirname "$0" )" && pwd )"
BUILD_DEPS_DIR="$PROJECT_DIR/build-deps/"

# Create temp directory for building/pulling external libs
if [ ! -d "$BUILD_DEPS_DIR" ]; then
sudo mkdir ${PROJECT_DIR}/build-deps/
fi

## Install dependencies required via apt-get
apt-get update
apt-get install -y  make                            \
                    automake                        \
                    autoconf                        \
                    libtool                         \
                    curl                            \
                    g++                             \
                    unzip

# Pull protobuf from github
cd ${PROJECT_DIR}/build-deps/
wget https://github.com/protocolbuffers/protobuf/archive/v3.7.1.tar.gz
tar -xvzf v3.7.1.tar.gz
cd protobuf-3.7.1

# build protobuf
./autogen.sh
./configure
make
make check
sudo make install


echo "Cleaning up temporary files.."
sudo rm -r ${PROJECT_DIR}/build-deps
echo "Cleaning up done"

# Regenerate cache and links to libs
sudo ldconfig