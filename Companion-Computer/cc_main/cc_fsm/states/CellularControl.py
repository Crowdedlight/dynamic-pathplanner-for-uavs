#!/usr/bin/env python

import smach


# define state
class CellularControl(smach.State):
    def __init__(self):
        # init smach state
        smach.State.__init__(self,
                             outcomes=['reset_mission'],
                             input_keys=['cellular_override', 'telemStatus', 'mqtt_client'],
                             output_keys=['cellular_override', 'telemStatus', 'mqtt_client'])

    def execute(self, userdata):
        print("Executing State CELLULAR_CONTROL")

        # setup stuff to control from cellular
        #todo main telemetry need to have a way to get override signal from webapi too... Consider making web-api in
        #todo another class and just invoke/import class here?

        # todo notify operator maybe? ask for what to do.

