#!/bin/bash

# source ros for smach before we can run python script
source /opt/ros/melodic/setup.bash

# cd into current dir
cd $(dirname $0)

#run python
exec python3 cc_main.py
