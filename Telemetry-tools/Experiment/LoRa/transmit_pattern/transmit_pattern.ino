/*****************************************************************************/
/* configuration */

/* #define DEBUG */
#define RF95_FREQ 433.775 /* 869.500  MHz */
#define RF95_TX_POWER 20 /* [5;23] dBm (corresponding to [3;200] mW) */
/****************************************************************************/
/* includes */
#include <SPI.h>
#include <RH_RF95.h>

/****************************************************************************/
/* defines */
#define LED_ONBOARD 13

/* rf95 radio defines */
#define RFM95_CS 8 /* for feather32u4 */
#define RFM95_RST 4 /* for feather32u4 */
#define RFM95_INT 7 /* for feather32u4 */

RH_RF95 rf95(RFM95_CS, RFM95_INT); /* Singleton instance of the radio driver */
#define RADIO_RXBUF_SIZE 200
#define RADIO_RECV_MAX_LEN 256 /* limited by the unsigned char len of RadioHead recv function */
#define RADIO_RXBUF_SIZE_SAFE (RADIO_RXBUF_SIZE + RADIO_RECV_MAX_LEN)
/****************************************************************************/
/* global variables */
unsigned long now; /* time since boot in milli seconds */
unsigned short i, j;
char result;

/* led */
unsigned long led_tout;
unsigned char led_state;

/* csv output */
unsigned long rssi_tout;

/* rf95 radio */
uint8_t radio_rxbuf[RADIO_RXBUF_SIZE_SAFE]; /* being overly cautious on the length on to prevent a crash */
uint8_t radio_cnt;
short last_rssi;


void setup() {
  // setup
  pinMode(LED_ONBOARD, OUTPUT);

  Serial.begin(57600);
  Serial.setTimeout(0);

  while (!rf95.init())
  {
    while (1);
  }

  /*
   * For maximum flexibility the user may decide on the spread spectrum modulation
   * bandwidth (BW), spreading factor (SF) and error correction rate (CR). (HopeRF p.9)
   * SF is defined in table HopeRF p.24, 7=128chips/symbol,9=512chips/symbol, 12=4096chips/symbol
  
  Bw125Cr45Sf128   Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on. Default medium range.
  Bw500Cr45Sf128   Bw = 500 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on. Fast+short range.
  Bw31_25Cr48Sf512 Bw = 31.25 kHz, Cr = 4/8, Sf = 512chips/symbol, CRC on. Slow+long range.
  Bw125Cr48Sf4096  Bw = 125 kHz, Cr = 4/8, Sf = 4096chips/symbol, CRC on. Slow+long range. 
  
  http://www.airspayce.com/mikem/arduino/RadioHead/classRH__RF95.html#ab9605810c11c025758ea91b2813666e3
  */
  if (!rf95.setModemConfig(RH_RF95::Bw125Cr48Sf4096))
  {
    while (1);
  }
  if (!rf95.setFrequency(RF95_FREQ))
  {
    while (1);
  }
  rf95.setTxPower(RF95_TX_POWER, false); 

}

int16_t packetnum = 0;  // packet counter, we increment per xmission

void loop() {
  /* housekeeping */
  now = millis();

  /* update LED */
  if (now > led_tout)
  {
    led_tout = now + 200;
    led_state = !led_state;
    digitalWrite(LED_ONBOARD, led_state);
  }

  /* transmit timeout */
  if (now > rssi_tout)
  {
    //next timeout in 500ms => 0.5s
    rssi_tout = now + 500;

    //transmit package number
    char radiopacket[5];
    itoa(packetnum++, radiopacket, 10);
    //radiopacket[0] = 0;
    
    rf95.send((uint8_t *)radiopacket, 5);
    rf95.waitPacketSent();
  }
}
