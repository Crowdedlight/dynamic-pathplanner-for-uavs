#!/usr/bin/env python
# RSSI production test

import serial, sys, optparse, time
from pexpect import fdpexpect
from datetime import datetime


class rssi_reporting():
    def __init__(self, device):

        self.port = serial.Serial(device, opts.baudrate, timeout=0,
                             dsrdtr=opts.dsrdtr, rtscts=opts.rtscts, xonxoff=opts.xonxoff)

        self.ser = fdpexpect.fdspawn(self.port.fileno(), logfile=sys.stdout, encoding="UTF-8")

        # open logfile
        fout = open('rssi_log.txt', 'a')

        # write timestamp
        time_str = datetime.now().strftime("%Y%m%d_%H:%M:%S")
        fout.write("\n" + "######################################################################" + "\n")
        fout.write(time_str + "\n")
        fout.write("######################################################################" + "\n")

        self.ser.logfile_read = fout

        # vars
        self.retried = False

        # enter AT Mode
        self.enterATMode()

        # test to get report every second for 10 seconds => works fine, but can't see or transmit to other radio
        # while in AT mode. So can't confirm packages myself
        i = 0
        while i < 20:
            # ask for report
            self.askForReport()

            # sleep 1 sec, and ask again
            time.sleep(1)

            i += 1

        # leave AT mode. Sleep 1 sec to ensure we are done with what we should get
        time.sleep(1)
        self.leaveATMode()

        # close port when we exit
        self.port.close()

    def leaveATMode(self):
        # leaving AT Mode
        self.ser.send("ATO\r\n")

        try:
            self.ser.expect(['ATO', 'Left AT Mode'], timeout=5)
        except fdpexpect.TIMEOUT:
            print("failed to leave AT mode")
            return

    def leaveATandRetry(self):
        # flag we are retrying
        self.retried = True

        # leave AT mode
        self.leaveATMode()

        # try again
        self.enterATMode()

    def enterATMode(self):
        # set in command mode
        self.ser.flush()
        self.ser.send('+++')

        # sleep 1 sec to let it enter AT Mode
        time.sleep(1)

        # expect "ok" when in command mode
        try:
            self.ser.expect(['OK', 'SiK .* on HM-TRP'], timeout=2)
        except fdpexpect.TIMEOUT:
            if self.retried:
                print("timeout")
                return
            else:
                print("timeout, trying to leave AT mode first")
                self.leaveATandRetry()
                return

        return True

    def askForReport(self):
        # ask for report, only prepend "\r\n" when last cmd is +++, otherwise just append it
        self.ser.send('\r\nATI7\r\n')
        # self.ser.send('\r\nATI5\r\n') # just used to see parameters of the radio

        # expect "ATI7" when in command mode
        try:
            self.ser.expect(['ATI7'], timeout=5)
        except fdpexpect.TIMEOUT:
            print("failed to ack rssi report")
            return

        # expect to get rssi report
        try:
            self.ser.expect(['dco='], timeout=5)
        except fdpexpect.TIMEOUT:
            print("failed to get actual rssi report")
            return


if __name__ == "__main__":

    parser = optparse.OptionParser("update_mode")
    parser.add_option("--baudrate", type='int', default=57600, help='baud rate')
    parser.add_option("--rtscts", action='store_true', default=False, help='enable rtscts')
    parser.add_option("--dsrdtr", action='store_true', default=False, help='enable dsrdtr')
    parser.add_option("--xonxoff", action='store_true', default=False, help='enable xonxoff')

    opts, args = parser.parse_args()

    if len(args) == 0:
        print("usage: rssi.py <DEVICE...>")
        sys.exit(1)

    for d in args:
        print("Getting RSSI and Noise report for %s" % d)
        myReport = rssi_reporting(d)
