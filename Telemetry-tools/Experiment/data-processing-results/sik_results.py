
import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import csv
import numpy as np
from math import log10, pow
from matplotlib.ticker import FormatStrFormatter
import re
from matplotlib.colors import ListedColormap, LinearSegmentedColormap

def calc_freepath_dist(damping, frequency):
    # formula from https://www.electronics-notes.com/articles/antennas-propagation/propagation-overview/free-space-path-loss.php
    # damping in db, distance in meters, frequency in MHz
    return float(pow(10, ((damping + 27.55 ) /20 - log10(frequency))))

def find_index_by_value(arr, value):
    for idx, item in enumerate(arr):
        if item >= value:
            return idx


# select the file
file = "china_antenna_mro_20190515_10_30_15"
# file = "procom_antenna_test_20190515_11_09_17"

# load the csv file
with open("../SiK/results/" + file + ".csv", 'r') as csvFile:
    reader = csv.reader(csvFile)

    # define the borders between each zone
    if file == "china_antenna_mro_20190515_10_30_15":
        zone_borders = [0, 76, 149, 245, 272.5, 308.5, 337, 366, 417, 427, 442, 456, 478, 506, 530, 538, 580] # china_antenna_mro
        zone_dampning = [00, 10, 20, 30, 40, 50, 60, 70, 80, 70, 71, 72, 73, 74, 75, 76]
        plotname = "SiK Module with China Antenna"
    elif file == "procom_antenna_test_20190515_11_09_17":
        zone_borders = [0, 35, 56, 65, 78, 85, 96, 110, 123, 132, 151, 165, 188, 209, 250] # procom_antenna_mro
        zone_dampning = [70, 80, 90, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90]
        plotname = "SiK Module with Procom Antenna"

    # Colour stuff to make equal mapping between borders and then normalizing from 0 to 1
    zone_color = plt.get_cmap("viridis", 512)
    zone_color = ListedColormap(zone_color(np.linspace(0.25, 0.55, 256)))
    color_norm_boundary = matplotlib.colors.BoundaryNorm(boundaries=zone_dampning, ncolors=len(zone_dampning))
    color_norm = matplotlib.colors.Normalize(vmin=0, vmax=len(zone_dampning))

    # hold center of every zone
    dist_center = []

    # calculate distance based on free path loss for each zone, save in array used for plotting.
    zone_text = []
    for zd in zone_dampning:
        # calculate free path loss distance based on damping
        zone_text.append("{0:.1f}".format(calc_freepath_dist(zd, 433)))

    # create figure
    fig, ax = plt.subplots(figsize=(19, 9))
    ax2 = ax.twiny()
    fig.suptitle(plotname, fontsize=14, fontweight='bold')
    ax.set_xlabel('Attenuation [dB]')
    ax.set_ylabel('RSSI [dB]')

    ax2.set_xlabel("The attenuation as theoretical FSPL distance [m]", labelpad=2)

    # draw axvspan for each zone
    for idx, zone in enumerate(zone_borders[:-1]):
        # first border is 0, next is what it ends at. So colour from current index to next index
        ax.axvspan(zone, zone_borders[idx +1], alpha=0.8, color=zone_color(color_norm(color_norm_boundary(zone_dampning[idx]))))

        # calculate center of zone
        center = ((zone_borders[idx +1] - zone) / 2) + zone
        dist_center.append(center)

    # top axis with distance
    ax2.set_xticks(dist_center)
    ax2.tick_params(labelrotation=50)
    ax2.set_xticklabels(zone_text)

    # bottom axis with attenuation
    ax.set_xticks(dist_center)
    ax.set_xticklabels(zone_dampning)

    # plot rssi for each entry in csv file.
    x_time = []
    rssi_y = []
    period_y = []
    fixed_y = []
    lost_y = []

    for row in reader:
        if float(row[0]) <= zone_borders[-1]:
            x_time.append(float(row[0]))
            rssi_y.append(float(row[3]))
            period_y.append(float(row[2]))
            fixed_y.append(float(row[8]))
            lost_y.append(float(row[10]))

    # zero calibrate fixed. Is needed as radio counts from the radio boot and not when we start to log data
    fixed_y[:] = [x - fixed_y[0] for x in fixed_y]

    # set start and end x
    ax.set_xlim(left=0, right=zone_borders[-1])
    ax2.set_xlim(ax.get_xlim())

    # plot lines
    ax.plot(x_time, rssi_y, color="red")

    # minor ticks so easier to see RSSI
    plt.minorticks_on()
    ax2.tick_params(axis='x', which='minor', bottom=False, top=False)

    ####################################################################
    # calculate average rssi for each zone using the borders and margins
    ####################################################################
    average_rssi_zones = []
    average_rssi_samples = []
    average_period = []
    lost_messages_in_period = []

    for idx, zone in enumerate(zone_borders[:-1]):
        # find index by value in time_x as we got the borders there
        start_idx = find_index_by_value(x_time, zone)
        end_idx = find_index_by_value(x_time, zone_borders[idx + 1])

        # get subset
        # print("zone: {}, next_zone: {}, start_idx: {}, end_idx: {}".format(zone, zone_borders[idx+1], start_idx, end_idx))

        rssi_subset = rssi_y[start_idx:end_idx]
        period_subset = period_y[start_idx:end_idx]
        lost_subset = lost_y[start_idx:end_idx]

        # remove first and last element as margin to ensure we are within the same zone
        rssi_subset = rssi_subset[1:-1]
        period_subset = period_subset[1:-1]

        # if size is not 0, get average
        if len(rssi_subset) == 0:
            average_rssi_zones.append(0)
            average_rssi_samples.append(0)
            average_period.append(0)
            lost_messages_in_period.append(-1)
        else:
            average_rssi_zones.append(sum(rssi_subset) / len(rssi_subset))
            average_rssi_samples.append(len(rssi_subset))
            average_period.append(sum(period_subset) / len(period_subset))
            lost_messages_in_period.append(lost_subset[-1] - lost_subset[0])

        # print(rssi_subset)
        # print("size: {}".format(len(rssi_subset)))

    # print average rssi for each zone & save to file
    with open('./sik_figures/' + file + '_table.txt', 'w') as f_table:
        for idx, zone in enumerate(zone_dampning):
            print("Zone: {0}db, FSPL dist: {1}m, samples: {2}, average_rssi: {3:.2f}, average_period: {4:.2f}, Lost messages: {5}"
                  .format(zone, zone_text[idx], average_rssi_samples[idx], average_rssi_zones[idx],
                          average_period[idx], lost_messages_in_period[idx]))

            f_table.write("Zone: {0}db, FSPL dist: {1}m, samples: {2}, average_rssi: {3:.2f}, average_period: {4:.2f}, Lost messages: {5}\n"
                          .format(zone, zone_text[idx], average_rssi_samples[idx], average_rssi_zones[idx],
                                  average_period[idx], lost_messages_in_period[idx]))

    # show figure
    plt.savefig("./sik_figures/" + file + ".png", facecolor=None, edgecolor=None, format="png", pad_inches=0.1)
    plt.savefig("./sik_figures/" + file + ".eps", facecolor=None, edgecolor=None, format="eps", pad_inches=0.1)
    plt.show()

    #####################################################################################
    ############################## PERIOD AND ERROR PLOTS ###############################
    #####################################################################################
    # show plot with two subplots. One being period and one being lost/corrected packages

    fig, (ax3, ax4) = plt.subplots(nrows=2, ncols=1, sharex="all", figsize=(19, 9))
    fig.suptitle(plotname, fontsize=14, fontweight='bold')

    ax3.plot(x_time, period_y, color="orange")

    ax3.set_ylabel('Period between received messages [s]')
    ax3_t = ax3.twiny()
    ax3_t.set_xlabel("The attenuation as theoretical FSPL distance [m]", labelpad=2)

    ax4.plot(x_time, fixed_y, color="yellow", label="Bit corrected messages")
    ax4.plot(x_time, lost_y, color="red", label="Lost messages")
    ax4.legend(loc="best")

    ax4.set_xlabel('Attenuation [dB]')
    ax4.set_ylabel('Count of messages')

    # draw axvspan for each zone
    for idx, zone in enumerate(zone_borders[:-1]):
        # first border is 0, next is what it ends at. So colour from current index to next index
        ax3.axvspan(zone, zone_borders[idx + 1], alpha=0.8, color=zone_color(color_norm(color_norm_boundary(zone_dampning[idx]))))
        ax4.axvspan(zone, zone_borders[idx + 1], alpha=0.8, color=zone_color(color_norm(color_norm_boundary(zone_dampning[idx]))))

    # top axis with distance
    ax3_t.set_xticks(dist_center)
    ax3_t.tick_params(labelrotation=50)
    ax3_t.set_xticklabels(zone_text)

    ax3.tick_params(
        axis='x',  # changes apply to the x-axis
        which='both',  # both major and minor ticks are affected
        bottom=False,  # ticks along the bottom edge are off
        top=False,  # ticks along the top edge are off
        labelbottom=False)  # labels along the bottom edge are off

    ax3.grid(color="black", alpha=0.2, axis="y")
    ax4.grid(color="black", alpha=0.3, axis="y")

    ax4.set_xticks(dist_center)
    ax4.set_xticklabels(zone_dampning)

    # set start and end x
    ax3.set_xlim(left=0, right=zone_borders[-1])
    ax3_t.set_xlim(ax3.get_xlim())

    plt.savefig("./sik_figures/" + file + "_more.png", facecolor=None, edgecolor=None, format="png", pad_inches=0.1)
    plt.savefig("./sik_figures/" + file + "_more.eps", facecolor=None, edgecolor=None, format="eps", pad_inches=0.1)
    plt.show()
