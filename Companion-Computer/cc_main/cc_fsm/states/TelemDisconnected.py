#!/usr/bin/env python

import smach
from cc_fsm.CustomDefines import *
from cc_fsm.protocol.cc_messages_pb2 import *


# define state
class TelemDisconnected(smach.State):
    def __init__(self):
        # init smach state
        smach.State.__init__(self,
                             outcomes=['cellular_control'],
                             input_keys=['telemStatus', 'mqtt_client'])

    def execute(self, userdata):
        print("Executing State TELEM_DISCONNECTED")

        # clean nextstate variable if we are revisiting the state
        self.nextState = ""

        # todo stop drone. Set to pos-hold mode.
        # todo move to cellular control when drone is confirmed to hold current pos



