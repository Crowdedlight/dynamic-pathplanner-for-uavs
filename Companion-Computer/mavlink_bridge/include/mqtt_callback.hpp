//
// Created by Frederik Mazur Andersen on 27/03/19.
//

#ifndef MAVLINK_BRIDGE_MQTT_CALLBACK_HPP
#define MAVLINK_BRIDGE_MQTT_CALLBACK_HPP

/**
 * Local callback & listener class for use with the client connection.
 * This is primarily intended to receive messages, but it will also monitor
 * the connection to the broker. If the connection is lost, it will attempt
 * to restore the connection and re-subscribe to the topic.
 */

/**
 * This class is built upon offical example at: https://github.com/eclipse/paho.mqtt.cpp/blob/master/src/samples/async_subscribe.cpp
 * It is changed to act as a interface. So main program inherits from this class and enable the overrides
 */

#include <mqtt/async_client.h>

class callback : public virtual mqtt::callback,
                 public virtual mqtt::iaction_listener

{
    // This deomonstrates manually reconnecting to the broker by calling
    // connect() again.
    virtual void reconnect() {}

    // Re-connection failure
    virtual void on_failure(const mqtt::token& tok) override {}

    // (Re)connection success
    // Either this or connected() can be used for callbacks.
    virtual void on_success(const mqtt::token& tok) override {}

    // (Re)connection success
    virtual void connected(const string& cause) override {}

    // Callback for when the connection is lost.
    // This will initiate the attempt to manually reconnect.
    virtual void connection_lost(const string& cause) override {}

    // Callback for when a message arrives.
    virtual void message_arrived(mqtt::const_message_ptr msg) override {}

    virtual void delivery_complete(mqtt::delivery_token_ptr token) override {}
};

#endif //MAVLINK_BRIDGE_MQTT_CALLBACK_HPP
