#!/usr/bin/env python
import threading

import rospy
import smach
from .CustomDefines import TelemetryStatus
from .states import *
from gcs_master.srv import *
from mavlink_lora.msg import mavlink_lora_heartbeat, mavlink_lora_mission_list
from gcs_master.msg import goal_pos

# make sm global so we can access
sm = smach.StateMachine(outcomes=[])

# heartbeat
hb_pub = rospy.Publisher("mavlink_heartbeat_tx", mavlink_lora_heartbeat, queue_size=1)


def handle_get_active_state(req):
    return sm.get_active_states()


def hb_send(event):
    # publish heartbeat
    hb_pub.publish(sm.userdata.heartbeat)


def main():
    # init node
    rospy.init_node('gcs_fsm')

    # create service-call for ros-tests to see current active state
    state_service = rospy.Service('gcs_master/get/active_state', active_state, handle_get_active_state)

    # variables for userdata
    sm.userdata.telemStatus = TelemetryStatus.disconnected
    sm.userdata.SOC = -1
    sm.userdata.armed = False
    sm.userdata.flightmode = "UNKNOWN"
    sm.userdata.curr_pos = [0, 0, 0]
    sm.userdata.goalPos = goal_pos()
    sm.userdata.gps_fix = -1
    sm.userdata.heartbeat = mavlink_lora_heartbeat()
    sm.userdata.new_mission_list = mavlink_lora_mission_list()

    # set heartbeat
    sm.userdata.heartbeat.type = 6 # GCS TYPE
    sm.userdata.heartbeat.autopilot = 8 # No autopilot, GCS
    sm.userdata.heartbeat.base_mode = 136 # Armed + guided mode
    sm.userdata.heartbeat.custom_mode = 0 #
    sm.userdata.heartbeat.system_status = 4 # ACTIVE
    sm.userdata.heartbeat.system_id = 255 # GCS

    # start heartbeat timer
    hb_timer = rospy.Timer(rospy.Duration(1), hb_send, oneshot=False)

    # open container
    with sm:
        # add states

        # BOOT
        smach.StateMachine.add('BOOT', Boot.Boot(),
                               transitions={'idle': 'IDLE'})

        # IDLE
        smach.StateMachine.add('IDLE', Idle.Idle(),
                               transitions={'pre-flight': 'PREFLIGHT'})

        # PREFLIGHT
        smach.StateMachine.add('PREFLIGHT', Preflight.Preflight(),
                               transitions={'preflight_failed': 'PREFLIGHT_FAILED',
                                            'takeoff': 'TAKEOFF'})

        # TAKEOFF
        smach.StateMachine.add('TAKEOFF', Takeoff.Takeoff(),
                               transitions={'flight': 'FLIGHT'})

        # FLIGHT
        smach.StateMachine.add('FLIGHT', Flight.Flight(),
                               transitions={'lost_datalink': 'LOST_DATALINK',
                                            'mission_upload': 'MISSION_UPLOAD',
                                            'land': 'LAND',
                                            'low_battery': 'LOW_BATTERY',
                                            'emergency_stop': 'EMERGENCY_STOP'})

        # LANDING
        smach.StateMachine.add('LAND', Land.Land(),
                               transitions={'post_flight': 'POST_FLIGHT'})

        # POST FLIGHT
        smach.StateMachine.add('POST_FLIGHT', PostFlight.PostFlight())

        # CELLULAR CONTROL
        smach.StateMachine.add('CELLULAR_CONTROL', CellularControl.CellularControl(),
                               transitions={'mission_upload': 'MISSION_UPLOAD'})

        # EMERGENCY LANDING
        smach.StateMachine.add('EMERGENCY_LANDING', EmergencyLanding.EmergencyLanding())

        # EMERGENCY STOP
        smach.StateMachine.add('EMERGENCY_STOP', EmergencyStop.EmergencyStop(),
                               transitions={'mission_upload': 'MISSION_UPLOAD',
                                            'emergency_landing': 'EMERGENCY_LANDING'})

        # LOST DATALINK
        smach.StateMachine.add('LOST_DATALINK', LostDatalink.LostDatalink(),
                               transitions={'cellular_control': 'CELLULAR_CONTROL'})

        # LOW BATTERY
        smach.StateMachine.add('LOW_BATTERY', LowBattery.LowBattery(),
                               transitions={'mission_upload': 'MISSION_UPLOAD',
                                            'emergency_stop': 'EMERGENCY_STOP'})

        # MISSION UPLOAD
        smach.StateMachine.add('MISSION_UPLOAD', MissionUpload.MissionUpload(),
                               transitions={'flight': 'FLIGHT',
                                            'emergency_stop': 'EMERGENCY_STOP'})

        # PREFLIGHT FAILED
        smach.StateMachine.add('PREFLIGHT_FAILED', PreflightFailed.PreflightFailed(),
                               transitions={'idle': 'IDLE'})

    # Create a thread to execute the smach container
    smach_thread = threading.Thread(target=sm.execute)
    smach_thread.start()

    # Wait for ctrl-c
    rospy.spin()

    # Request the container to preempt
    sm.request_preempt()

    # Block until everything is preempted
    # (you could do something more complicated to get the execution outcome if you want it)
    smach_thread.join()