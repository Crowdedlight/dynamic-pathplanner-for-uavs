#!/bin/bash

# Determine ROS_DISTRO
#---------------------
ROS_DISTRO=$(ls /opt/ros/)

if [[ -z "${ROS_DISTRO}" ]]; then
  echo "No ROS distribution was found in /opt/ros/. Aborting!"
  exit 1
fi

# Enable global C++11 if required by the user
#--------------------------------------------
if [[ "${GLOBAL_C11}" == "true" ]]; then
  echo "Enabling C++11 globally"
  export CXXFLAGS="${CXXFLAGS} -std=c++11"
fi

# Install catkin tools # https://catkin-tools.readthedocs.io/en/latest/installing.html
#---------------------
#apt-get install -qq wget
#sh -c 'echo "deb http://packages.ros.org/ros/ubuntu `lsb_release -sc` main" > /etc/apt/sources.list.d/ros-latest.list'
#wget http://packages.ros.org/ros.key -O - | apt-key add -
#apt-get update
#apt-get install -qq python-catkin-tools xterm
#export TERM="xterm"
#-----------------------
# Due to a bug in catkin tools and the project being on "life-support" with little to no support from author, CI moved
# to catkin_build instead. In anyway catkin_make should be more efficient for CI purposes. It does require this
# environment setting otherwise it fucks up the python_path in its cache file and can't import the modules used
export CATKIN_SETUP_UTIL_ARGS=--extend

# Install ROS packages required by the user
#------------------------------------------
# Split packages into package list
IFS=' ' read -ra PACKAGES <<< "${ROS_PACKAGES_TO_INSTALL}"
# Clear packages list
ROS_PACKAGES_TO_INSTALL=""

# Append "ros-kinetic-" (eg for Kinetic) before the package name
# and append in the packages list
for package in "${PACKAGES[@]}"; do
  ROS_PACKAGES_TO_INSTALL="${ROS_PACKAGES_TO_INSTALL} ros-${ROS_DISTRO}-${package}"
done

# Install the packages
apt-get install -qq ${ROS_PACKAGES_TO_INSTALL}

# Display system information
#---------------------------
echo "##############################################"
uname -a
lsb_release -a
gcc --version
echo "CXXFLAGS = ${CXXFLAGS}"
cmake --version
echo "##############################################"

# Prepare build
#--------------
# https://docs.gitlab.com/ce/ci/variables/README.html#predefined-variables-environment-variables

cd ${CI_PROJECT_DIR}/..
mkdir -p catkin_workspace/src

####### NESSECARY AS THIS DEPENDS ON MSGS FROM THE OTHER PACKAGE ###########
# clone mavlink lora
git clone "https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/Crowdedlight/mavlink_lora.git"

# move ros part into catkin workspace
cp -r mavlink_lora/ros/mavlink_lora/ catkin_workspace/src/

# Copy current directory into a src directory
# Don't move the original clone or GitLab CI fails!
cp -r ${CI_PROJECT_DIR}/ROS-packages/. catkin_workspace/src/

# move catkin workspace into project_dir
cp -rl ${CI_PROJECT_DIR}/../catkin_workspace/ ${CI_PROJECT_DIR}/

# debug where we are? 
ls -al

# go to workspace for rosdep
cd ${CI_PROJECT_DIR}/catkin_workspace/

# debug where we are? 
ls -al

if [[ ("${USE_ROSDEP}" != false && ! -z "${USE_ROSDEP}") || -z "${USE_ROSDEP}" ]]; then
  echo "Using rosdep to install dependencies"
  # Install rosdep and initialize
  apt-get update
  apt-get install -qq python-rosdep python-pip
  rosdep init || true
  rosdep update

  # Use rosdep to install dependencies
  rosdep install --from-paths src --ignore-src --rosdistro ${ROS_DISTRO} -y --as-root apt:false
fi

