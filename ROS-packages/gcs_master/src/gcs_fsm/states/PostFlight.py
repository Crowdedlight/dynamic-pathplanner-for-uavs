#!/usr/bin/env python

import rospy
import smach
import threading
from ..CustomDefines import TelemetryStatus
from mavlink_lora.msg import mavlink_lora_heartbeat


# define state
class PostFlight(smach.State):

    # INIT
    def __init__(self):
        # init smach state
        smach.State.__init__(self,
                             input_keys=['telemStatus'],
                             output_keys=['telemStatus'])

        # Vars
        self.ud = None

        # workaround to change state from callback functions
        self.nextState = ""
        self.mutex = threading.Lock()
        self.rate = rospy.Rate(10)
        self.hb_sub = None

    def execute(self, userdata):
        rospy.loginfo("Executing State POST-FLIGHT")

        # save userdata so class can use it
        self.ud = userdata

        # clean nextstate variable if we are revisiting the state
        self.nextState = ""

        # setup subs
        self.hb_sub = rospy.Subscriber("mavlink_heartbeat_rx", mavlink_lora_heartbeat, self.heartbeat_callback)

        while not rospy.is_shutdown():

            # If stuff is initalized and telemetry connected change state
            self.mutex.acquire()

            if self.nextState != '':
                self.on_state_transition()
                return self.nextState

            self.mutex.release()

            self.rate.sleep()

    def on_state_transition(self):
        # unregister all subs and release all mutexes
        self.hb_sub.unregister()
        self.mutex.release()

    # CALLBACKS
    def heartbeat_callback(self, msg):
        # mark time now
        now = rospy.get_time()


# todo no idea what to do here yet... just have it as a end state. Maybe check some flight stuff otherwise return back to
#  IDLE?