//
// Created by Crow on 25-03-2019.
//

#include "../include/mavlink_bridge.hpp"

mavlink_bridge::mavlink_bridge() {}

int mavlink_bridge::init(string serial_fc, int baud_fc, string serial_gcs, int baud_gcs)
{
    // init the two lora libs
    int result = 0;

    int fc_result = lora_lib_FC.init("FC", serial_fc, baud_fc, bind(&mavlink_bridge::parse_msg_from_fc, this, placeholders::_1));
    int gcs_result = lora_lib_GCS.init("GCS", serial_gcs, baud_gcs, bind(&mavlink_bridge::parse_msg_from_gcs, this, placeholders::_1));

    //check if we opened ports successfully
    if (fc_result == -1 || gcs_result == -1)
        result = -1;

    // MQTT init
    cout << "Initializing MQTT for server '" << SERVER_ADDRESS << "'..." << endl;
    mqtt_client = std::make_shared<mqtt::async_client>(SERVER_ADDRESS, CLIENT_ID);

    connOpts.set_keep_alive_interval(20);
    connOpts.set_clean_session(true);


    //pointers to topics
    mission_item_single_pub     = make_shared<mqtt::topic>(*mqtt_client, "mission/upload/single_item", QOS_least_once, true);
    mission_ack_pub             = make_shared<mqtt::topic>(*mqtt_client, "mission/ack", QOS_excatly_once, true);
    mission_current_pub         = make_shared<mqtt::topic>(*mqtt_client, "mission/current", QOS_least_once, true);
    cmd_ack_pub                 = make_shared<mqtt::topic>(*mqtt_client, "cmd/ack", QOS_excatly_once, true);
    heartbeat_rx_pub            = make_shared<mqtt::topic>(*mqtt_client, "heartbeat_rx", QOS_least_once, true);
    mission_count_pub           = make_shared<mqtt::topic>(*mqtt_client, "mission/upload/count", QOS_least_once, true);
    mission_partial_write_pub   = make_shared<mqtt::topic>(*mqtt_client, "mission/upload/partial_write", QOS_least_once, true);

    //SUBS
    sub_topics_QOS1 = {"heartbeat_tx", "mission/upload/request_item"};
    sub_topics_QOS2 = {"mission/upload", "mission/ack", "cmd/reposition", "cmd/land", "cmd/takeoff", "cmd/clear_mission",
                  "cmd/set_mode"};

    //Bind callback to ourselves as we have overridden the mqtt_callback class
    mqtt_client->set_callback(*this);

    //Try to connect
    try {
        cout << "Connecting to the MQTT server..." << endl;
        mqtt_client->connect(connOpts);

        //wait for connection established
        while (!mqtt_client->is_connected()){}
    }
    catch (const mqtt::exception&) {
        cerr << "\nERROR: Unable to connect to MQTT server: '"
             << SERVER_ADDRESS << "'" << endl;
        result = -1;
    }

    //Send commands to limit msg stream to 0.5s intervals
    lora_lib_FC.ml_queue_msg_command_long(MAVLINK_MSG_ID_SET_MESSAGE_INTERVAL, 24, 500000, 0, 0, 0, 0, 0, 0); //GPS RAW
    lora_lib_FC.ml_queue_msg_command_long(MAVLINK_MSG_ID_SET_MESSAGE_INTERVAL, 42, 500000, 0, 0, 0, 0, 0, 0); //MISSION CURRENT
    lora_lib_FC.ml_queue_msg_command_long(MAVLINK_MSG_ID_SET_MESSAGE_INTERVAL, 111, 500000, 0, 0, 0, 0, 0, 0); //TIMESYNC

    return result;
}
void mavlink_bridge::update()
{
    //only update FC with 200hz, as telemetry can't handle more than 100hz
    lora_lib_FC.update();

    update_cnt++;

    //updates with 100hz
    if (update_cnt % 2 == 0)
        lora_lib_GCS.update();
}
void mavlink_bridge::shutdown()
{
    lora_lib_GCS.shutdown();
    lora_lib_FC.shutdown();

    try {
        cout << "\nDisconnecting from the MQTT server..." << endl;

        mqtt_client->disconnect();
        cout << "OK" << endl;
    }
    catch (const mqtt::exception& exc) {
        cerr << exc.what() << endl;
    }

}
/************** From GCS *************/
void mavlink_bridge::parse_msg_from_gcs(unsigned char *msg)
{
    last_heard = std::chrono::system_clock::now();

    bool send_drone = true;

    // extract message info
    struct mavlink_msg_t m;
    m.payload_len = msg[ML_POS_PAYLOAD_LEN];
    m.seq = msg[ML_POS_PACKET_SEQ];
    m.sys_id = msg[ML_POS_SYS_ID];
    m.comp_id = msg[ML_POS_COMP_ID];
    m.msg_id = msg[ML_POS_MSG_ID];

    int i;
    for (i=0; i<m.payload_len; i++)
        m.payload.push_back(msg[ML_POS_PAYLOAD + i]);

    unsigned char crc_lsb = msg[6 + m.payload_len];
    unsigned char crc_msb = msg[7 + m.payload_len];
    m.checksum = (8 << crc_msb) | crc_lsb;

    // handle heartbeat messages
    if (m.msg_id == MAVLINK_MSG_ID_HEARTBEAT)
    {
        mavlink_heartbeat_t hb = mavlink_lora_lib::ml_unpack_msg_heartbeat(&m.payload.front());

        //mqtt msg
        heartbeat hb_msg;

        hb_msg.set_autopilot(hb.autopilot);
        hb_msg.set_base_mode(hb.base_mode);
        hb_msg.set_custom_mode(hb.custom_mode);
        hb_msg.set_type(hb.type);
        hb_msg.set_system_id(hb.system_id);
        hb_msg.set_system_status(hb.system_status);

        //publish on mqtt
        heartbeat_rx_pub->publish(hb_msg.SerializeAsString());
    }

    //handle single missions
    if (m.msg_id == MAVLINK_MSG_ID_MISSION_ITEM_INT)
    {
        //unpack msg
        mavlink_mission_item_int_t item = mavlink_lora_lib::ml_unpack_msg_mission_item_int(&m.payload.front());
        cout << "Mission Item Int received with seq: " << item.seq << endl;

        //pack in mqtt msg
        mission_item missionItem;
        missionItem.set_param1(item.param1);
        missionItem.set_param2(item.param2);
        missionItem.set_param3(item.param3);
        missionItem.set_param4(item.param4);
        missionItem.set_seq(item.seq);
        missionItem.set_command(item.command);
        missionItem.set_frame(item.frame);
        missionItem.set_x(item.x);
        missionItem.set_y(item.y);
        missionItem.set_z(item.z);
        missionItem.set_autocontinue(item.autocontinue);
        missionItem.set_current(item.current);
        missionItem.set_target_system(item.target_system);
        missionItem.set_target_component(item.target_component);

        //publish msg
        mission_item_single_pub->publish(missionItem.SerializeAsString());

        //don't forward this msg TODO might want to forward them?
        send_drone = false;
    }

    //handle mission count
    if (m.msg_id == MAVLINK_MSG_ID_MISSION_COUNT)
    {
        //unpack msg
        mavlink_mission_count_t item = mavlink_lora_lib::ml_unpack_msg_mission_count(&m.payload.front());
        cout << "Mission Count received: " << item.count << endl;

        //pack in mqtt format
        mission_count mqtt_item;
        mqtt_item.set_count(item.count);
        mqtt_item.set_target_component(item.target_component);
        mqtt_item.set_target_system(item.target_system);

        //publish message
        mission_count_pub->publish(mqtt_item.SerializeAsString());

        //don't forward this msg
        send_drone = false;
    }

    //handle mission partial write list
    if (m.msg_id == MAVLINK_MSG_ID_MISSION_WRITE_PARTIAL_LIST)
    {
        //unpack msg
        mavlink_mission_partial_write_list_t item = mavlink_lora_lib::ml_unpack_msg_mission_partial_write_list(&m.payload.front());
        cout << "Mission write partial list received. Start index:  " << item.start_index << ", end index: " << item.end_index << endl;

        //pack in mqtt format
        mission_write_partial_list mqtt_item;
        mqtt_item.set_start_index(item.start_index);
        mqtt_item.set_end_index(item.end_index);
        mqtt_item.set_target_component(item.target_component);
        mqtt_item.set_target_system(item.target_system);

        //publish message
        mission_partial_write_pub->publish(mqtt_item.SerializeAsString());

        //don't forward this msg
        send_drone = false;
    }


    // if msg hasn't been handled in some way and no flag to not send it to drone has appeared send it to drone
    if (send_drone)
    {
        lora_lib_FC.ml_forward_msg(msg);
    }

}
/***************************************************************************/
/************** From FC *************/
void mavlink_bridge::parse_msg_from_fc(unsigned char *msg)
{
    last_heard = std::chrono::system_clock::now();

    bool send_gcs = true;

    // extract message info
    struct mavlink_msg_t m;
    m.payload_len = msg[ML_POS_PAYLOAD_LEN];
    m.seq = msg[ML_POS_PACKET_SEQ];
    m.sys_id = msg[ML_POS_SYS_ID];
    m.comp_id = msg[ML_POS_COMP_ID];
    m.msg_id = msg[ML_POS_MSG_ID];

    for (auto i=0; i<m.payload_len; i++)
        m.payload.push_back(msg[ML_POS_PAYLOAD + i]);

    unsigned char crc_lsb = msg[6 + m.payload_len];
    unsigned char crc_msb = msg[7 + m.payload_len];
    m.checksum = (8 << crc_msb) | crc_lsb;


//    mavlink_lora::mavlink_lora_msg m;
//    m.header.stamp = last_heard;


    //do not forward messages that isn't important to us, sort them out here.
    // TODO for now we don't send mission requests to GCS, as it should be handled through CC
    int filter_msg[] = {2, 30, 31, 32, 35, 36, 65, 69, 70, 74, 83, 105, 140, 141, 147, 230, 241,  245, 40, 51};

    for (auto id : filter_msg)
        if (m.msg_id == id)
            send_gcs = false;

    // handle state messages
    if	(m.msg_id == MAVLINK_MSG_ID_SYS_STATUS)
    {
        last_heard_sys = last_heard;
        mavlink_sys_status_t sys_status = mavlink_lora_lib::ml_unpack_msg_sys_status (&m.payload.front());
        sys_status_voltage_battery = sys_status.voltage_battery;
        sys_status_battery_remaining = sys_status.battery_remaining;
        sys_status_cpu_load = sys_status.load;

//        ml_send_status_msg();
    }

    //Handle Mission upload messages
    /* Forcing it to use INT variants for best possible precision. */
    if (m.msg_id == MAVLINK_MSG_ID_MISSION_REQUEST_INT || m.msg_id == MAVLINK_MSG_ID_MISSION_REQUEST)
    {
        //todo We probably should not do this if we want the option to directly upload missions from GCS
        // without using custom protocol...
        //if mission upload flag isn't set, then we don't react on those. If we did we could throw exception.
        if (mission_uploading)
        {
            //we don't want to let these go to GCS, as it is ours
            send_gcs = false;

            // stop mission ack timeout
//            timer_control.remove(mission_ack_timeout);
            timer_control.remove(mission_up_timeout);

            //reset retries
            mission_retries = 0;

            //unpack and update requested seq
            mavlink_mission_request_int_t request = mavlink_lora_lib::ml_unpack_msg_mission_request_int(&m.payload.front());

            std::cout << "Item requested from FC:" << request.seq << std::endl;

            // Possible multipath causes to receive multiple requests
            if(mission_up_index == request.seq)
                printf("Already sent this sequence number once\n");
            else
            {
                //reset retries
                mission_retries = 0;

                mission_up_index = request.seq;

                //send next item
                ml_send_mission_item_int();

                //start timer
//                mission_ack_timeout = timer_control.add(milliseconds(MISSION_ITEM_TIMEOUT_TIME), [&](CppTime::timer_id) { mission_ack_timeout_callback(); });
            }
        }
    }

    //handle mission ack
    if (m.msg_id == MAVLINK_MSG_ID_MISSION_ACK)
    {
        //stop timer
        timer_control.remove(mission_up_timeout);
        timer_control.remove(mission_ack_timeout);

        //reset retries
        mission_retries = 0;

        //unpack
        mavlink_mission_ack_t ack = mavlink_lora_lib::ml_unpack_msg_mission_ack(&m.payload.front());

        mission_uploading = false;

        //respond back with result
        mission_ack mis_ack;
        mis_ack.set_result(ack.type);
        mis_ack.set_result_text(mission_result_parser(ack.type));

        //MQTT publish?
        mission_ack_pub->publish(mis_ack.SerializeAsString());

        // DEBUG
        send_gcs = false;
        std::cout << "Mission ack from FC: " << mis_ack.result_text() << std::endl;
    }

    //handle command ack
    if (m.msg_id == MAVLINK_MSG_ID_COMMAND_ACK)
    {
        //stop timer
        timer_control.remove(command_timeout);

        //reset confirmation
        confirmation = 0;

        //unpack
        mavlink_command_ack_t ack = mavlink_lora_lib::ml_unpack_msg_command_ack(&m.payload.front());

        //respond back with result
        command_ack ack_msg;
        ack_msg.set_command(ack.command);
        ack_msg.set_result(ack.result);
        ack_msg.set_result_text(command_result_parser(ack.result));

        //publish
        cmd_ack_pub->publish(ack_msg.SerializeAsString());

        //DEBUG
        std::cout << ack_msg.result_text() << std::endl;
    }

    //handle current mission
    if (m.msg_id == MAVLINK_MSG_ID_MISSION_CURRENT)
    {
        // make msg to MQTT format and send
        uint16_t seq = mavlink_lora_lib::ml_unpack_msg_mission_current(&m.payload.front());

        mission_current mis_curr;
        mis_curr.set_seq(seq);

        // publish on mqtt
        try {

            mission_current_pub->publish(mis_curr.SerializeAsString());
        } catch (exception &ex)
        {
            cout << ex.what() << endl;
        }
    }

    //if not filtered, send it
    if (send_gcs)
    {
        lora_lib_GCS.ml_forward_msg(msg);
    }
}
/***************************** COMMAND LONG ********************************/
void mavlink_bridge::ml_send_command_long(unsigned short cmd_id, float p1, float p2, float p3, float p4, float p5, float p6, float p7)
{
    std::cout << "Sending Command_long with id: " << std::to_string(cmd_id) << ", and confirmation: " << std::to_string(confirmation) << std::endl;

    //save command as last command
    last_cmd_long.command = cmd_id;
    last_cmd_long.param1 = p1;
    last_cmd_long.param2 = p2;
    last_cmd_long.param3 = p3;
    last_cmd_long.param4 = p4;
    last_cmd_long.param5 = p5;
    last_cmd_long.param6 = p6;
    last_cmd_long.param7 = p7;

    //queue msg, can only be sent to drone
    lora_lib_FC.ml_queue_msg_command_long(cmd_id, p1, p2, p3, p4, p5, p6, p7, confirmation);

    //start timeout
    command_timeout = timer_control.add(milliseconds(DEFAULT_TIMEOUT_TIME), [&](CppTime::timer_id) { command_long_timeout_callback(); });
}
/***************************************************************************/
void mavlink_bridge::command_long_timeout_callback()
{
    //if timeout triggers, increment confirmation and resend last command
    confirmation++;

    ml_send_command_long(last_cmd_long.command, last_cmd_long.param1, last_cmd_long.param2, last_cmd_long.param3, last_cmd_long.param4, last_cmd_long.param5, last_cmd_long.param6, last_cmd_long.param7 );
}
/***************************************************************************/
string mavlink_bridge::command_result_parser(uint8_t result)
{
    switch(result)
    {
        case 0:
            return "MAV_RESULT_ACCEPTED";
        case 1:
            return "MAV_RESULT_TEMPORARILY_REJECTED";
        case 2:
            return "MAV_RESULT_DENIED";
        case 3:
            return "MAV_RESULT_UNSUPPORTED";
        case 4:
            return "MAV_RESULT_FAILED";
        case 5:
            return "MAV_RESULT_IN_PROGRESS";
        default:
            return "DIDN'T RECOGNIZE RESULT CODE";
    }
}
/******************************* MISSIONS **********************************/
void mavlink_bridge::ml_new_mission_callback(const vector<mavlink_mission_item_int_t> & waypoints)
{

    //Received new mission on topic, start uploading
    mission_up_count = waypoints.size();
    mission_up_index = -1;
    missionlist = waypoints;

    std::cout << "New mission to FC. Length:" << std::to_string(mission_up_count) << std::endl;

    //Set status to uploading mission. Currently not used, but maybe use it to make sure you can't upload a new mission while another mission gets uploaded?
    mission_uploading = true;

    //Send mission count
    ml_send_mission_count();
}
void mavlink_bridge::ml_send_single_mission_item_int(mavlink_mission_item_int_t item)
{
    //send mission item. can only go to FC
    lora_lib_FC.ml_queue_msg_mission_item_int(item.param1, item.param2, item.param3, item.param4, item.x, item.y, item.z, item.seq, item.command, item.frame, item.current, item.autocontinue);

    //start timeout
    mission_up_timeout = timer_control.add(milliseconds(MISSION_ITEM_SINGLE_TIMEOUT_TIME), [&](CppTime::timer_id) { mission_upload_single_item_timeout_callback(); });
}
void mavlink_bridge::ml_mission_clear_all_callback()
{
    //queue command for clearing all missions
    ml_send_mission_clear_all();
}
void mavlink_bridge::ml_send_mission_count()
{
    std::cout << "Sending mission count" << std::endl;

    // queue msg, can only go to FC
    lora_lib_FC.ml_queue_msg_mission_count(mission_up_count);

    // start timer
    mission_up_timeout = timer_control.add(milliseconds(DEFAULT_TIMEOUT_TIME), [&](CppTime::timer_id) { mission_up_count_timeout_callback(); });
}
void mavlink_bridge::ml_send_mission_clear_all()
{
    std::cout << "Sending mission clear all" << std::endl;

    // queue msg, only goes to FC
    lora_lib_FC.ml_queue_msg_mission_clear_all();

    // start timer
    mission_up_timeout = timer_control.add(milliseconds(DEFAULT_TIMEOUT_TIME), [&](CppTime::timer_id) { mission_clear_all_timeout_callback(); });
}
void mavlink_bridge::ml_send_mission_item_int()
{
    //send mission item.
    mavlink_mission_item_int_t item = missionlist[mission_up_index];

    lora_lib_FC.ml_queue_msg_mission_item_int(item.param1, item.param2, item.param3, item.param4, item.x, item.y, item.z, item.seq, item.command, item.frame, item.current, item.autocontinue);

    //start timeout
    mission_up_timeout = timer_control.add(milliseconds(MISSION_ITEM_TIMEOUT_TIME), [&](CppTime::timer_id) { mission_up_item_timeout_callback(); });
    cout << "Sent mission item seq: " << item.seq << endl;
}
/***************************************************************************/
void mavlink_bridge::mission_up_item_timeout_callback()
{
    //increment retries
    mission_retries++;

    //check if retries has been exceeded
    if (mission_retries > MISSION_MAX_RETRIES)
    {
        //publish error on ack topic
        mission_ack msg;
        msg.set_result(20);
        msg.set_result_text(mission_result_parser(20));

        //publish on MQTT, so CC_FSM know it failed
        mission_ack_pub->publish(msg.SerializeAsString());

        //cancel command
        mission_up_index = -1;
        mission_up_count = 0;
        missionlist.clear();
        mission_uploading = false;

        //reset retries
        mission_retries = 0;

        //debug
        std::cout << "MAX RETRIES REACHED" << std::endl;

        return;
    }

    //if timeout triggers, resend last mission item
    ml_send_mission_item_int();
}
void mavlink_bridge::mission_upload_single_item_timeout_callback()
{
    //increment retries
    mission_retries++;

    //check if retries has been exceeded
    if (mission_retries > MISSION_MAX_RETRIES)
    {
        //publish error on ack topic
        mission_ack msg;
        msg.set_result(20);
        msg.set_result_text(mission_result_parser(20));

        //publish on MQTT
        mission_ack_pub->publish(msg.SerializeAsString());

        //cancel command
        mission_uploading = false;

        //reset retries
        mission_retries = 0;

        //debug
        printf("MAX RETRIES REACHED\n");

        return;
    }

    //if timeout triggers, resend last mission item
    ml_send_single_mission_item_int(last_single_mission_upload);
}
void mavlink_bridge::mission_clear_all_timeout_callback()
{
    //increment retries
    mission_retries++;

    //check if retries has been exceeded
    if (mission_retries > MISSION_MAX_RETRIES)
    {
        //publish error on ack topic
        mission_ack msg;
        msg.set_result(20);
        msg.set_result_text(mission_result_parser(20));

        //publish on MQTT
        mission_ack_pub->publish(msg.SerializeAsString());

        //reset retries
        mission_retries = 0;

        //debug
        printf("MAX RETRIES REACHED\n");

        return;
    }

    //if timeout triggers, resend
    ml_send_mission_clear_all();
}
/***************************************************************************/
void mavlink_bridge::mission_up_count_timeout_callback()
{
    //increment retries
    mission_retries++;

    //check if retries has been exceeded
    if (mission_retries > MISSION_MAX_RETRIES)
    {
        //publish error on ack topic
        mission_ack msg;
        msg.set_result(20);
        msg.set_result_text(mission_result_parser(20));

        //publish on MQTT
        mission_ack_pub->publish(msg.SerializeAsString());

        //cancel command
        mission_uploading = false;

        //reset retries
        mission_retries = 0;

        //debug
        printf("MAX RETRIES REACHED\n");

        return;
    }

    //if timeout triggers, resend count message
    ml_send_mission_count();
}
/***************************************************************************/
void mavlink_bridge::mission_ack_timeout_callback()
{
    //publish error on ack topic
    mission_ack msg;
    msg.set_result(21);
    msg.set_result_text(mission_result_parser(21)); //ack timeout error

    //MQTT publish
    mission_ack_pub->publish(msg.SerializeAsString());

    printf("Mission ACK timed out\n");
}
/***************************************************************************/
string mavlink_bridge::mission_result_parser(uint8_t result)
{
    switch(result)
    {
        case 0:
            return "MAV_MISSION_ACCEPTED";
        case 1:
            return "MAV_MISSION_ERROR";
        case 2:
            return "MAV_MISSION_UNSUPPORTED_FRAME";
        case 3:
            return "MAV_MISSION_UNSUPPORTED";
        case 4:
            return "MAV_MISml_command_preflight_calibration_compassSION_NO_SPACE";
        case 5:
            return "MAV_MISSION_INVALID";
        case 6:
            return "MAV_MISSION_INVALID_PARAM1";
        case 7:
            return "MAV_MISSION_INVALID_PARAM2";
        case 8:
            return "MAV_MISSION_INVALID_PARAM3";
        case 9:
            return "MAV_MISSION_INVALID_PARAM4";
        case 10:
            return "MAV_MISSION_INVALID_PARAM5_X";
        case 11:
            return "MAV_MISSION_INVALID_PARAM6_Y";
        case 12:
            return "MAV_MISSION_INVALID_PARAM7";
        case 13:
            return "MAV_MISSION_INVALID_SEQUENCE";
        case 14:
            return "MAV_MISSION_DENIED";
        case 20:
            return "MAV_MISSION_MAX_RETRIES"; //Custom error indicating aborting due to max retries with no success
        case 21:
            return "MAV_MISSION_ACK_TIMEOUT"; //Timeout on ack for mission
        case 22:
            return "MAV_MISSION_APPEND_ACCEPTED"; //Custom ack for appending a single mission upload
        case 23:
            return "MAV_MISSION_ITEM_RECEIVED";
        default:
            return "DIDN'T RECOGNIZE RESULT CODE";
    }
}
/************************* INTERFACE COMMANDS ******************************/
void mavlink_bridge::command_arm_disarm(bool arm)
{
    //arm if true, disarm if false
    //send msg
    ml_send_command_long(MAVLINK_MSG_ID_COMPONENT_ARM_DISARM, arm, 0, 0, 0, 0, 0, 0);
}
void mavlink_bridge::command_start_mission(int first_item, int last_item)
{
    //start mission, first and last item as parameter
    //send msg
    ml_send_command_long(MAVLINK_MSG_ID_MISSION_START, first_item, last_item, 0, 0, 0, 0, 0);
}
void mavlink_bridge::command_set_mode(float mode, float custom_mode, float custom_sub_mode)
{
    ml_send_command_long(MAVLINK_MSG_ID_DO_SET_MODE, mode, custom_mode, custom_sub_mode, 0, 0, 0, 0);
}
void mavlink_bridge::command_land(float abort_alt, float precision_land_mode, float yaw_angle, float lat, float lon, float altitude)
{
    ml_send_command_long(MAVLINK_MSG_ID_NAV_LAND, abort_alt, precision_land_mode, 0, yaw_angle, lat, lon, altitude);
}
void mavlink_bridge::command_do_reposition(float ground_speed, float yaw_heading, float lat, float lon, float alt)
{
    //
    ml_send_command_long(MAVLINK_MSG_ID_SET_REPOSITION, ground_speed, 0, 0, yaw_heading, lat, lon, alt);
}
void mavlink_bridge::command_do_pause_continue(const bool continue_mission)
{
    // 1 == continue mission, 0 == hold position
    ml_send_command_long(MAVLINK_MSG_ID_DO_PAUSE_CONTINUE, continue_mission, 0, 0, 0, 0, 0, 0);
}
/***************************** HEARTBEAT ***********************************/
void mavlink_bridge::ml_send_heartbeat_callback(const mavlink_heartbeat_t &msg)
{
    //queue heartbeat

    //todo should we send to both?, only sending to GCS atm
    lora_lib_GCS.ml_queue_msg_heartbeat(msg.type, msg.autopilot, msg.base_mode, msg.custom_mode, msg.system_status, msg.system_id);
}

void mavlink_bridge::reconnect() {
    this_thread::sleep_for(chrono::milliseconds(1500));
    try {
        mqtt_client->connect(connOpts, nullptr, *this);
    }
    catch (const mqtt::exception& exc) {
        cerr << "Error: " << exc.what() << endl;
        exit(1);
    }
}
/***************************** MQTT Callback/Connections Overrides ***********************************/
void mavlink_bridge::on_failure(const mqtt::token &tok) {
    cout << "Connection attempt failed" << endl;

    // if we keep failing over 50 times with no success stop trying to reconnect?
    // keep running program as GCS to FC passthrough could still work, heartbeats from CC would stop
    if (nretry < N_RETRY_ATTEMPTS)
        reconnect();
}

void mavlink_bridge::connected(const string& cause) {
    cout << "\nConnection success" << endl;

    // resubscribe to topics QOS1
    for (auto const &top : sub_topics_QOS1)
    {
        mqtt_client->subscribe(top, QOS_least_once);
        cout << "subscribed to topic with QOS1: " << top << endl;
    }

    // resubscribe QOS2
    for (auto const &top : sub_topics_QOS2)
    {
        mqtt_client->subscribe(top, QOS_excatly_once);
        cout << "subscribed to topic with QOS2: " << top << endl;
    }

}

void mavlink_bridge::connection_lost(const string &cause) {
    cout << "\nConnection lost" << endl;
    if (!cause.empty())
        cout << "\tcause: " << cause << endl;

    cout << "Reconnecting..." << endl;
    nretry = 0;
    reconnect();
}

void mavlink_bridge::message_arrived(mqtt::const_message_ptr msg) {
    //print
//    cout << "topic: " << msg->get_topic() << endl;

    //switch case going to functions based on topic
    if(msg->get_topic() == "mission/upload")
    {
        //parse msg
        mission_list mission;
        mission.ParseFromString(msg->get_payload_str());

        //todo confirm the parsing of waypoints works as intended
        cout << "UPLOAD MISSION TO FC WAYPOINTS SIZE: " << mission.waypoints().size() << endl;

        //copy waypoints to vector. Reserve space for efficient push_backs
        vector<mavlink_mission_item_int_t> waypoints;
        waypoints.reserve(mission.waypoints_size());

        // change to mavlink_item for correct field sizes. As queue function expects items to be certain bytes
        // TODO might not be needed, but done to be safe
        for (auto const &item : mission.waypoints())
        {
            mavlink_mission_item_int_t newItem;
            newItem.param1 = item.param1();
            newItem.param2 = item.param2();
            newItem.param3 = item.param3();
            newItem.param4 = item.param4();
            newItem.command = item.command();
            newItem.frame = item.frame();
            newItem.seq = item.seq();
            newItem.autocontinue = item.autocontinue();
            newItem.current = item.current();
            newItem.x = item.x();
            newItem.y = item.y();
            newItem.z = item.z();
            newItem.target_component = item.target_component();
            newItem.target_system = item.target_system();

            // save in vector
            waypoints.push_back(newItem);
        }

        //start upload to FC
        ml_new_mission_callback(waypoints);

    }
    else if (msg->get_topic() == "mission/ack")
    {
        //parse msg
        mission_ack ack;
        ack.ParseFromString(msg->get_payload_str());
        cout << "mission ack received over MQTT with result: " << ack.result() << ", (" << ack.result_text() << ")" << endl;

        //output to GCS
        lora_lib_GCS.ml_queue_msg_mission_ack(ack.result());
        cout << "sent ack to GCS" << endl;
    }
    else if(msg->get_topic() == "heartbeat_tx")
    {
        //parse msg
        heartbeat hb;
        hb.ParseFromString(msg->get_payload_str());

        //change to mavlink format for proper queueing
        mavlink_heartbeat_t mav_hb;
        mav_hb.base_mode = hb.base_mode();
        mav_hb.autopilot = hb.autopilot();
        mav_hb.custom_mode = hb.custom_mode();
        mav_hb.type = hb.type();
        mav_hb.system_status = hb.system_status();
        mav_hb.system_id = hb.system_id();

        //output heartbeat to GCS
        ml_send_heartbeat_callback(mav_hb);
    }
    else if(msg->get_topic() == "cmd/reposition")
    {
        //parse msg
        cmd_reposition cmd;
        cmd.ParseFromString(msg->get_payload_str());

        //send reposition command
        command_do_reposition(cmd.ground_speed(), cmd.yaw_heading(), cmd.lat(), cmd.lon(), cmd.alt());
    }
    else if(msg->get_topic() == "cmd/land")
    {
        //parse msg
        cmd_land cmd;
        cmd.ParseFromString(msg->get_payload_str());

        //send land command
        command_land(cmd.abort_alt(), cmd.precision_landing_mode(), cmd.yaw_angle(), cmd.lat(), cmd.lon(), cmd.alt());
    }
    else if(msg->get_topic() == "cmd/takeoff")
    {
        //parse msg TODO
        //send command takeoff
    }
    else if(msg->get_topic() == "cmd/clear_mission")
    {
        //parse msg
        cmd_clear_missions cmd;
        cmd.ParseFromString(msg->get_payload_str());

        //send command clear mission
        ml_mission_clear_all_callback();
    }
    else if(msg->get_topic() == "cmd/set_mode")
    {
        //parse msg
        cmd_set_mode cmd;
        cmd.ParseFromString(msg->get_payload_str());

        //send command clear mission
        command_set_mode(cmd.mode(), cmd.custom_mode(), cmd.custom_sub_mode());
    }
    else if (msg->get_topic() == "mission/upload/request_item")
    {
        mission_request_int request;
        request.ParseFromString(msg->get_payload_str());

        //send to GCS
        lora_lib_GCS.ml_queue_msg_mission_request(request.seq());
    }
}