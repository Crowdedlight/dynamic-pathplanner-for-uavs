#!/usr/bin/env python

import smach
import threading
from datetime import datetime
import time
from copy import deepcopy
from cc_fsm.CustomDefines import *
import cc_fsm.protocol.cc_messages_pb2 as messages


# define state
class NewMission(smach.State):
    def __init__(self):
        # init smach state
        smach.State.__init__(self,
                             outcomes=['telemetry_control'],
                             input_keys=['missionList', 'mqtt_client', 'mission_count'],
                             output_keys=['missionList', 'mqtt_client'])

        # vars
        self.ud = None

        # workaround to change state from callback functions
        self.nextState = ""
        self.mutex = threading.Lock()

        self.item_receive_timer = None
        self.upload_timeout = 1.5  # max 2 sec, between items is assumed. We ask for next item if longer
        self.sent_mission_to_fc = False
        self.mission_count = 0
        self.mission_next_item = 0
        self.new_mission_list = []
        self.retries = 0

        # setup subs
        self.client = None

    def on_state_transition(self):
        # release mqtt callback?
        self.client.message_callback_remove("mission/single_item")
        self.client.message_callback_remove("mission/ack")

        # release mutex and stop timers
        self.mutex.release()
        self.item_receive_timer.cancel()


    def execute(self, userdata):
        print("Executing State NEW_MISSION", flush=True)

        self.ud = userdata
        self.client = self.ud.mqtt_client

        # clean nextstate variable if we are revisiting the state
        self.nextState = ""
        self.sent_mission_to_fc = False
        self.mission_count = self.ud.mission_count.count
        self.mission_next_item = 0
        self.new_mission_list = []
        self.retries = 0

        # setup subscribers,
        # All Mavlink messages => passthrough
        self.client.message_callback_add("mission/upload/single_item", self.msg_mission_item_callback)
        self.client.message_callback_add("mission/ack", self.msg_mission_ack_callback)

        # debug
        print("Mission length: {0}".format(self.mission_count), flush=True)

        # send request for next sequence
        self.sendItemRequest()

        while True:
            # next state check
            self.mutex.acquire()
            if self.nextState != "":
                self.on_state_transition()
                return self.nextState

            self.mutex.release()

            # sleep 100 ms. should mean we check with 10hz, which is more than enough
            time.sleep(0.1)

    def sendItemRequest(self):
        self.mutex.acquire()

        # todo put in max retries of 5. Only tries to send request 5 times, before it decides that communication is lost and abort the upload
        if self.retries >= 5:
            print("MAX RETRIES, ABORTING", flush=True)
            self.nextState = "telemetry_control"
            self.mutex.release()
            return

        # send request for next mission item
        item = messages.mission_request_int()
        item.seq = self.mission_next_item

        payload = bytearray(item.SerializeToString())
        self.client.publish("mission/upload/request_item", payload, qos=1)

        print("Mission request for item {0} sent".format(item.seq), flush=True)

        self.retries += 1

        # start timer for resend if we haven't received next item yet
        self.item_receive_timer = threading.Timer(self.upload_timeout, self.sendItemRequest)
        self.item_receive_timer.start()

        self.mutex.release()

    def msg_mission_item_callback(self, client, userdata, msg):
        self.mutex.acquire()

        # reset timer and retires
        self.item_receive_timer.cancel()
        self.retries = 0

        # parse msg
        item = messages.mission_item()
        item.ParseFromString(msg.payload)

        # if item we expected, save it and increment and ask for next item
        if item.seq == self.mission_next_item:
            self.new_mission_list.append(item)
            self.mission_next_item += 1
            print("Received and appended mission item with seq: {}".format(item.seq), flush=True)

        # release mutex before calling new function
        self.mutex.release()

        if self.mission_next_item == self.mission_count and not self.sent_mission_to_fc:
            # we got all items, upload to FC
            self.send_mission_FC()
        elif not self.sent_mission_to_fc:
            # send next request
            self.sendItemRequest()


    def msg_mission_ack_callback(self, client, userdata, msg):
        # todo this is broadcast down to groundstation too. Is final response telling that we finished and
        #  should change back to telemControl state
        # TODO Is also here we should finally save new mission into missionList

        # parse msg
        ack = messages.mission_ack()
        ack.ParseFromString(msg.payload)

        if self.sent_mission_to_fc:
            self.mutex.acquire()
            # todo react on failure besides just printing?
            print(ack.result_text)

            # if success
            if ack.result == 0:
                # overwrite current missionlist
                self.ud.missionList = deepcopy(self.new_mission_list)
                self.nextState = 'telemetry_control'
            else:
                print("Mission upload to FC Failed", flush=True)
                self.nextState = 'telemetry_control'

            self.mutex.release()

    def send_mission_FC(self):
        print("ALL MISSION ITEMS RECEIVED", flush=True)
        # we got full mission. Make it into right format and publish it. Then set
        #  flag that the next mission_ack is response to our full upload to FC
        self.mutex.acquire()

        msg = messages.mission_list()
        msg.waypoints.extend(self.new_mission_list)

        # set flag
        self.sent_mission_to_fc = True

        self.mutex.release()

        # publish, using bytearray to insure no funny stuff with utf8 encoding and strings
        payload = bytearray(msg.SerializeToString())
        self.client.publish("mission/upload", payload, qos=2)