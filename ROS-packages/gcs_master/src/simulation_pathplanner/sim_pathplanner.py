
import rospy
from mavlink_lora.msg import mavlink_lora_mission_list, mavlink_lora_mission_item_int
from gcs_master.msg import goal_pos

class PathplannerNode:

    def __init__(self):
        # init
        self.missionList = mavlink_lora_mission_list()
        self.missionList.append_start_index = -1

        rospy.init_node("SIM_pathplanner")
        self.rate = rospy.Rate(1)

        # subs
        self.make_path_sub = rospy.Subscriber("pathplanner/mission/new", goal_pos, self.new_mission_callback)

        # pubs
        self.send_mission_pub = rospy.Publisher('pathplanner/mission/list', mavlink_lora_mission_list, queue_size=1)
        self.send_goal_pos_pub = rospy.Publisher('gcs_master/mission/new_goal', goal_pos, queue_size=1)

        rospy.sleep(0.1)

    def new_mission_callback(self, msg):
        # path is hardcoded, so just upload that

        # sleep 3 seconds as simulation.
        print("New mission goal is received.")
        rospy.sleep(3)

        print("Sending mission list")
        self.send_mission_pub.publish(self.missionList)

    def run(self):
        # run

        # TODO mission assumes we are at airport
        # make mission and save in var
        home_lat = 55.3104821
        home_lon = 10.4643509

        turn_lat = 55.3100344
        turn_lon = 10.4627190

        turn_lat2 = 55.3109203
        turn_lon2 = 10.4622987

        # TAKEOFF waypoint
        way1 = mavlink_lora_mission_item_int()
        way1.target_system = 0
        way1.target_component = 0
        way1.seq = 0
        way1.frame = 6  # global pos, relative alt_int
        way1.command = 22
        way1.x = int(home_lat * 10000000)
        way1.y = int(home_lon * 10000000)
        way1.z = 15
        way1.param1 = 5
        way1.current = 1
        way1.autocontinue = 1

        # WAYPOINT 1
        way2 = mavlink_lora_mission_item_int()
        way2.target_system = 0
        way2.target_component = 0
        way2.seq = 1
        way2.frame = 6  # global pos, relative alt_int
        way2.command = 16
        way2.param1 = 0  # hold time
        way2.param2 = 5  # acceptance radius in m
        way2.param3 = 0  # pass though waypoint, no trajectory control
        way2.x = int(turn_lat * 10000000)
        way2.y = int(turn_lon * 10000000)
        way2.z = 15
        way2.autocontinue = 1

        # # WAYPOINT 2
        way3 = mavlink_lora_mission_item_int()
        way3.target_system = 0
        way3.target_component = 0
        way3.seq = 2
        way3.frame = 6  # global pos, relative alt_int
        way3.command = 16
        way3.param1 = 0  # hold time
        way3.param2 = 5  # acceptance radius in m
        way3.param3 = 0  # pass though waypoint, no trajectory control
        way3.x = int(turn_lat2 * 10000000)
        way3.y = int(turn_lon2 * 10000000)
        way3.z = 15
        way3.autocontinue = 1
        #
        # # WAYPOINT 3
        # way4 = mavlink_lora_mission_item_int()
        # way4.target_system = 0
        # way4.target_component = 0
        # way4.seq = 3
        # way4.frame = 6  # global pos, relative alt_int
        # way4.command = 16
        # way4.param1 = 0  # hold time
        # way4.param2 = 5  # acceptance radius in m
        # way4.param3 = 0  # pass though waypoint, no trajectory control
        # way4.x = 55.4721370 * 10000000
        # way4.y = 10.4164410 * 10000000
        # way4.z = 15
        # way4.autocontinue = 1

        # Before land
        way6 = mavlink_lora_mission_item_int()
        way6.target_system = 0
        way6.target_component = 0
        way6.seq = 3
        way6.frame = 6  # global pos, relative alt_int
        way6.command = 16
        way6.param1 = 0  # hold time
        way6.param2 = 5  # acceptance radius in m
        way6.param3 = 0  # pass though waypoint, no trajectory control
        way6.x = int(home_lat * 10000000)
        way6.y = int(home_lon * 10000000)
        way6.z = 15
        way6.autocontinue = 1

        # WAYPOINT 4 LANDING
        way5 = mavlink_lora_mission_item_int()
        way5.target_system = 0
        way5.target_component = 0
        way5.seq = 4
        way5.frame = 6  # global pos, relative alt_int
        way5.command = 21
        way5.param1 = 5  # abort alt
        way5.param2 = 0  # precision landing. 0 = normal landing
        way5.x = int(home_lat * 10000000)
        way5.y = int(home_lon * 10000000)
        way5.z = 15
        way5.autocontinue = 1

        # add waypoints to list
        self.missionList.waypoints.append(way1)
        self.missionList.waypoints.append(way2)
        self.missionList.waypoints.append(way3)
        # self.missionList.waypoints.append(way4)
        self.missionList.waypoints.append(way6)
        self.missionList.waypoints.append(way5)

        # send new goal simulation
        pos = goal_pos()
        pos.lat = 0
        pos.lon = 0

        rospy.sleep(2)
        self.send_goal_pos_pub.publish(pos)

        while not rospy.is_shutdown():
            rospy.spin()


if __name__ == '__main__':
    obj = PathplannerNode()
    obj.run()
