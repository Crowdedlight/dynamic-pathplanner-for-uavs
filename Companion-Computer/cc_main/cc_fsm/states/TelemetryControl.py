#!/usr/bin/env python
import time
from datetime import datetime
import threading

import smach
from cc_fsm.CustomDefines import *
import cc_fsm.protocol.cc_messages_pb2 as messages


class TelemetryControl(smach.State):
    def __init__(self):
        # init smach state
        smach.State.__init__(self,
                             outcomes=['new_mission', 'append_mission', 'cellular_control', 'telemetry_disconnected'],
                             input_keys=['telemStatus', 'mqtt_client', 'SOC', 'flightmode', 'armed', 'mission_count', 'mission_append'],
                             output_keys=['telemStatus', 'mqtt_client', 'SOC', 'flightmode', 'armed', 'mission_count', 'mission_append'])

        # vars
        self.ud = None
        self.heartbeat_last_received = datetime.now()
        self.heartbeat_missed_timeout = 10

        # workaround to change state from callback functions
        self.nextState = ""
        self.mutex = threading.Lock()

        # setup subs
        self.client = None

    def on_state_transition(self):
        self.mutex.release()
        # release mqtt callback?
        self.client.message_callback_remove("heartbeat_rx")
        self.client.message_callback_remove("mission/upload/count")
        self.client.message_callback_remove("mission/upload/partial_write")

    def msg_mission_count_callback(self, client, userdata, msg):
        # parse msg
        item = messages.mission_count()
        item.ParseFromString(msg.payload)

        print("Mission count received of {0} items".format(item.count), flush=True)

        self.mutex.acquire()

        # if mission count is zero, don't do anything
        if item.count == 0:
            self.mutex.release()
            print("Mission count was ZERO, nothing to receive", flush=True)
            return

        # Save mission count
        self.ud.mission_count = item

        # change state
        self.nextState = 'new_mission'

        self.mutex.release()

    def msg_mission_partial_write_callback(self, client, userdata, msg):
        # parse msg
        item = messages.mission_write_partial_list()
        item.ParseFromString(msg.payload)

        self.mutex.acquire()

        # Save mission count
        self.ud.mission_append = item

        # change state
        self.nextState = 'append_mission'

        self.mutex.release()

    def heartbeat_callback(self, client, userdata, msg):
        # update last received time
        self.mutex.acquire()

        # parse msg
        hb = messages.heartbeat()
        hb.ParseFromString(msg.payload)

        self.ud.flightmode = decode_custom_flightmode(hb.custom_mode)
        self.ud.armed = decode_armed(hb.base_mode)

        self.heartbeat_last_received = datetime.now()

        self.mutex.release()

    def execute(self, userdata):
        print("Executing State Telemetry Control", flush=True)
        print(userdata.telemStatus, flush=True)

        self.ud = userdata
        self.client = self.ud.mqtt_client

        # clean nextstate variable if we are revisiting the state
        self.nextState = ""

        # setup subscribers,
        # All Mavlink messages => passthrough
        # mission items => change state and pass first item along
        self.client.message_callback_add("heartbeat_rx", self.heartbeat_callback)
        self.client.message_callback_add("mission/upload/count", self.msg_mission_count_callback)
        self.client.message_callback_add("mission/upload/partial_write", self.msg_mission_partial_write_callback)

        # Cellular Override msg => change state

        while True:

            # next state check
            self.mutex.acquire()
            if self.nextState != "":
                self.on_state_transition()
                return self.nextState

            # TODO calculation is not right, fix it
            # check how long since last time we got heartbeat. If over 10sec change state
            period = datetime.now() - self.heartbeat_last_received
            if period.seconds >= self.heartbeat_missed_timeout:
                # change state and set connection as disconnected
                # self.nextState = "telemetry_disconnected"
                self.ud.telemStatus = TelemetryStatus.disconnected

            self.mutex.release()

            # sleep 100 ms. should mean we check with 10hz, which is more than enough
            time.sleep(0.1)
