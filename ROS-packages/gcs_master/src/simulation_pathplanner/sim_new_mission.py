
import rospy
from mavlink_lora.msg import mavlink_lora_mission_list, mavlink_lora_mission_item_int, mavlink_lora_mission_ack
from gcs_master.msg import goal_pos

class PathplannerNode:

    def __init__(self):
        # init
        self.missionList = mavlink_lora_mission_list()
        self.missionList.append_start_index = -1

        rospy.init_node("SIM_append_mission")
        self.rate = rospy.Rate(1)

        # subs
        self.ack_sub = rospy.Subscriber("mavlink_interface/mission/ack", mavlink_lora_mission_ack, self.mission_ack_callback)

        # pubs
        self.new_mission_pub = rospy.Publisher('pathplanner/path/new', mavlink_lora_mission_list, queue_size=1)

        rospy.sleep(0.1)

    def mission_ack_callback(self, msg):
        # path is hardcoded, so just upload that

        # sleep 3 seconds as simulation.
        print("ACK received: {0}".format(msg.result_text))
        exit(0)

    def run(self):
        # run

        # TODO mission assumes we are at airport
        # make mission and save in var
        # this should replace the last two waypoints with two new waypoints
        home_lat = 55.3103449
        home_lon = 10.4644400

        # WAYPOINT 2
        way2 = mavlink_lora_mission_item_int()
        way2.target_system = 0
        way2.target_component = 0
        way2.seq = 0
        way2.frame = 6  # global pos, relative alt_int
        way2.command = 16
        way2.param1 = 0  # hold time
        way2.param2 = 5  # acceptance radius in m
        way2.param3 = 0  # pass though waypoint, no trajectory control
        way2.x = int(home_lat * 10000000)
        way2.y = int(home_lon * 10000000)
        way2.z = 15
        way2.autocontinue = 1

        # WAYPOINT 3 LANDING
        way5 = mavlink_lora_mission_item_int()
        way5.target_system = 0
        way5.target_component = 0
        way5.seq = 1
        way5.frame = 6  # global pos, relative alt_int
        way5.command = 21
        way5.param1 = 5  # abort alt
        way5.param2 = 0  # precision landing. 0 = normal landing
        way5.x = int(home_lat * 10000000)
        way5.y = int(home_lon * 10000000)
        way5.z = 15
        way5.autocontinue = 1

        # add waypoints to list
        # self.missionList.waypoints.append(way1)
        self.missionList.waypoints.append(way2)
        # self.missionList.waypoints.append(way3)
        # self.missionList.waypoints.append(way4)
        # self.missionList.waypoints.append(way6)
        self.missionList.waypoints.append(way5)

        # for append operations give start_index in missionlist
        self.missionList.append_start_index = 3


        rospy.sleep(2)
        self.new_mission_pub.publish(self.missionList)

        while not rospy.is_shutdown():
            rospy.spin()


if __name__ == '__main__':
    obj = PathplannerNode()
    obj.run()
