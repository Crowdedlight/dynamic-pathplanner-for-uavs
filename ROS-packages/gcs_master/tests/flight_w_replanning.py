#!/usr/bin/env python

import rospy
from gcs_master.srv import active_state
import unittest
from mavlink_lora.msg import mavlink_lora_heartbeat, mavlink_lora_status, mavlink_lora_mission_ack, mavlink_lora_command_start_mission, mavlink_lora_command_set_mode, mavlink_lora_command_ack, mavlink_lora_mission_item_int, mavlink_lora_mission_list, mavlink_lora_gps_raw
from std_msgs.msg import UInt16
from gcs_master.msg import goal_pos
from gcs_fsm.CustomDefines import *

#######################################################################################################################
# Test will take 30sec to run due to timing constraints. It tests going from boot -> post-flight on a normal flight with
# a to b flying. No errors or replannings happens
#######################################################################################################################


class test_flight_w_replanning(unittest.TestCase):
    def test_flight_w_replanning(self):

        # vars
        self.heartbeat_msg = mavlink_lora_heartbeat()
        # create msg
        self.heartbeat_msg.type = 13            # hexacopter
        self.heartbeat_msg.autopilot = 12       # PX4 autopilot
        self.heartbeat_msg.base_mode = 64       # manual disarmed
        self.heartbeat_msg.custom_mode = PX4_CustomMainMode.PX4_CUSTOM_MAIN_MODE_MANUAL
        self.heartbeat_msg.system_status = 4    # standby => System is grounded and on standby. It can be launched

        # setup subs
        rospy.Subscriber("mavlink_interface/command/set_mode", mavlink_lora_command_set_mode, self.setmode_callback)
        rospy.Subscriber("mavlink_interface/command/start_mission", mavlink_lora_command_start_mission, self.startmission_callback)

        # setup pubs
        self.heartbeat_pub = rospy.Publisher('mavlink_heartbeat_rx', mavlink_lora_heartbeat, queue_size=1)
        self.newgoal_pub = rospy.Publisher('gcs_master/mission/new_goal', goal_pos, queue_size=1)
        self.status_pub = rospy.Publisher('mavlink_status', mavlink_lora_status, queue_size=1)
        self.mission_ack_pub = rospy.Publisher('mavlink_interface/mission/ack', mavlink_lora_mission_ack, queue_size=1)
        self.command_ack_pub = rospy.Publisher('mavlink_interface/command/ack', mavlink_lora_command_ack, queue_size=1)
        self.mission_current_pub = rospy.Publisher("mavlink_interface/mission/current", UInt16, queue_size=1)
        self.mission_list_pub = rospy.Publisher("pathplanner/mission/list", mavlink_lora_mission_list, queue_size=1)
        self.gps_raw_pub = rospy.Publisher("mavlink_gps_raw", mavlink_lora_gps_raw, queue_size=1)
        self.new_path_pub = rospy.Publisher("pathplanner/path/new", mavlink_lora_mission_list, queue_size=1)

        # send heartbeat with 1hz with timer
        self.heartbeat_timer = rospy.Timer(rospy.Duration(1), self.send_heartbeat, oneshot=False)
        # self.heartbeat_timer.run()

        self.finished = False

        # execute test
        self.execute()

    def send_heartbeat(self, event):
        # publish
        self.heartbeat_pub.publish(self.heartbeat_msg)

    def execute(self):
        rospy.loginfo("Starting test case")
        # sleep for 10sec to ensure other node is ready and moved to idle state
        rospy.sleep(10)

        self.preflight()

        while not self.finished:
            rospy.sleep(2)

        # should now be in post flight => call service
        rospy.wait_for_service('gcs_master/get/active_state')
        state_service = rospy.ServiceProxy('gcs_master/get/active_state', active_state)

        # get response
        resp1 = state_service()

        rospy.loginfo("Final State = " + resp1.active_state)

        self.assertEqual(resp1.active_state, "POST_FLIGHT", "Active state is: " + resp1.active_state)

    def flight(self):
        # every 5 sec iterate though items. => send new mission current
        mis1 = UInt16().data = 0
        self.mission_current_pub.publish(mis1)

        rospy.loginfo("Sent MISSION_CURRENT = 0")

        rospy.sleep(3)

        mis2 = UInt16().data = 1
        self.mission_current_pub.publish(mis2)

        rospy.loginfo("Sent MISSION_CURRENT = 1")

        rospy.sleep(3)

        rospy.loginfo("NEW PATH FROM PATHPLANNER. REPLANNING")
        # make new path
        new_path = mavlink_lora_mission_list()

        item1 = mavlink_lora_mission_item_int()
        item1.command = MAVLINK_CMD_NAV_WAYPOINT
        item1.seq = 0
        item1.frame = MavFrame.MAV_FRAME_GLOBAL_RELATIVE_ALT_INT
        item1.x = int(10000000 * 55.474680)
        item1.y = int(10000000 * 10.322671)
        item1.z = 40

        item2 = mavlink_lora_mission_item_int()
        item2.command = MAVLINK_CMD_NAV_WAYPOINT
        item2.seq = 1
        item2.frame = MavFrame.MAV_FRAME_GLOBAL_RELATIVE_ALT_INT
        item2.x = int(10000000 * 55.479180)
        item2.y = int(10000000 * 10.320266)
        item2.z = 40

        item3 = mavlink_lora_mission_item_int()
        item3.command = MAVLINK_CMD_NAV_LAND
        item3.seq = 0
        item3.frame = MavFrame.MAV_FRAME_GLOBAL_RELATIVE_ALT_INT
        item3.x = int(10000000 * 55.470180)
        item3.y = int(10000000 * 10.321266)
        item3.z = 40

        new_path.waypoints.append(item1)
        new_path.waypoints.append(item2)
        new_path.waypoints.append(item3)
        self.new_path_pub.publish(new_path)

        # sleep for 2 sec and then send mission ack
        rospy.sleep(2)
        self.send_mission_ack()

        rospy.sleep(2)

        # continue flight from new waypoint 0
        mis1 = UInt16().data = 0
        self.mission_current_pub.publish(mis1)

        rospy.loginfo("Sent MISSION_CURRENT = 0")

        rospy.sleep(3)

        mis2 = UInt16().data = 1
        self.mission_current_pub.publish(mis2)

        rospy.loginfo("Sent MISSION_CURRENT = 1")

        rospy.sleep(3)

        mis3 = UInt16().data = 2
        self.mission_current_pub.publish(mis3)

        rospy.loginfo("Sent MISSION_CURRENT = 2")

        # wait 5sec => change heartbeat to show disarmed to show we are landed
        rospy.sleep(3)

        rospy.loginfo("Changed heartbeat to AUTO - DISARMED")
        self.heartbeat_msg.base_mode = 92  # auto - disarmed

        rospy.sleep(2)

        self.finished = True

    def startmission_callback(self, msg):
        rospy.loginfo("Received start mission command")
        rospy.sleep(1)
        # master will start_mission => respond with command_accepted
        msg_start_mission_ack = mavlink_lora_command_ack()
        msg_start_mission_ack.command = MAVLINK_CMD_MISSION_START_ID
        msg_start_mission_ack.result = CommandResult.MAV_RESULT_ACCEPTED
        msg_start_mission_ack.result_text = "MAV_RESULT_ACCEPTED"
        self.command_ack_pub.publish(msg_start_mission_ack)

        rospy.loginfo("Sent start mission ACK")

        # update heartbeat
        self.heartbeat_msg.base_mode = 220  # auto - armed
        self.heartbeat_msg.system_status = 5  # active => flying and engaged motors

        rospy.loginfo("Changed heartbeat to AUTO - ARMED and ACTIVE")

        rospy.sleep(1)
        self.flight()

    def setmode_callback(self, msg):
        rospy.loginfo("Received setmode command")
        # master will setmode => respond with command_accepted
        rospy.sleep(1)
        msg_command_ack = mavlink_lora_command_ack()
        msg_command_ack.result = CommandResult.MAV_RESULT_ACCEPTED
        msg_command_ack.result_text = "MAV_RESULT_ACCEPTED"
        msg_command_ack.command = MAVLINK_CMD_SET_MODE_ID
        self.command_ack_pub.publish(msg_command_ack)

        rospy.loginfo("Sent SetMode ACK")

        # change heartbeat
        self.heartbeat_msg.custom_mode = PX4_CustomMainMode.PX4_CUSTOM_MAIN_MODE_AUTO << 16 | PX4_CustomSubMode.PX4_CUSTOM_SUB_MODE_AUTO_MISSION << 24

        rospy.loginfo("Changed heartbeat to MISSION MODE")

    def send_mission_ack(self):
        # respond with mission_accepted
        msg_mission_ack = mavlink_lora_mission_ack()
        msg_mission_ack.result = MissionResult.MAV_MISSION_ACCEPTED
        msg_mission_ack.result_text = "MAV_MISSION_ACCEPTED"
        self.mission_ack_pub.publish(msg_mission_ack)

        rospy.loginfo("Sent ACK for mission upload")


    def preflight(self):
        # send goalpos
        msg_goal = goal_pos()
        msg_goal.lat = 55.472180  # airport
        msg_goal.lon = 10.324266  # airport
        self.newgoal_pub.publish(msg_goal)

        rospy.loginfo("Sent Goal pos")

        # sleep for 3sec
        rospy.sleep(1)

        # send status of 100 SOC
        msg_status = mavlink_lora_status()
        msg_status.batt_remaining = 100
        self.status_pub.publish(msg_status)

        rospy.loginfo("Sent 100 SOC")

        rospy.sleep(1)

        # send gps_fix
        gps_msg = mavlink_lora_gps_raw()
        gps_msg.fix_type = GpsFix.GPS_FIX_TYPE_3D_FIX
        self.gps_raw_pub.publish(gps_msg)

        rospy.loginfo("Sent 3D GPS FIX")

        rospy.sleep(2)

        # send route (3 points, takeoff -> fly to goal -> land)
        # create vector of three mission items, then publish mission_list back to preflight
        missionList = mavlink_lora_mission_list()

        item1 = mavlink_lora_mission_item_int()
        item1.command = MAVLINK_CMD_NAV_TAKEOFF
        item1.seq = 0
        item1.frame = MavFrame.MAV_FRAME_GLOBAL_RELATIVE_ALT_INT
        item1.x = int(10000000 * 55.471680)
        item1.y = int(10000000 * 10.324671)
        item1.z = 30

        item2 = mavlink_lora_mission_item_int()
        item2.command = MAVLINK_CMD_NAV_WAYPOINT
        item2.seq = 1
        item2.frame = MavFrame.MAV_FRAME_GLOBAL_RELATIVE_ALT_INT
        item2.x = int(10000000 * 55.472180)
        item2.y = int(10000000 * 10.324266)
        item2.z = 30

        item3 = mavlink_lora_mission_item_int()
        item3.command = MAVLINK_CMD_NAV_LAND
        item3.seq = 0
        item3.frame = MavFrame.MAV_FRAME_GLOBAL_RELATIVE_ALT_INT
        item3.x = int(10000000 * 55.472180)
        item3.y = int(10000000 * 10.324266)
        item3.z = 30

        missionList.waypoints.append(item1)
        missionList.waypoints.append(item2)
        missionList.waypoints.append(item3)

        # send mission back
        self.mission_list_pub.publish(missionList)
        rospy.loginfo("Sent mission list from pathplanner")

        rospy.sleep(2)

        # mission ack
        self.send_mission_ack()


if __name__ == '__main__':
    import rostest

    # init node
    rospy.init_node('flight_w_replanning_test')

    rostest.rosrun("test-gcs_master", 'test_flight_w_replanning', test_flight_w_replanning)
