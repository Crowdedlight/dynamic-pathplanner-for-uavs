#!/usr/bin/env python

import rospy
import smach
import threading
from ..CustomDefines import *
from ..HeartbeatMonitor import HeartbeatMonitor
from mavlink_lora.msg import mavlink_lora_heartbeat


# define state
class CellularControl(smach.State):

    # INIT
    def __init__(self):
        # init smach state
        smach.State.__init__(self,
                             outcomes=['mission_upload'],
                             input_keys=['telemStatus', 'missionList'],
                             output_keys=['telemStatus', 'missionList'])

        # Vars
        self.ud = None

        # workaround to change state from callback functions
        self.nextState = ""
        self.mutex = threading.Lock()
        self.rate = rospy.Rate(10)

        # setup heartbeat monitoring
        # self.monitorThread = HeartbeatMonitor(self.heartbeat_lost)
        # self.monitorThread.setName("Heartbeat Monitor")

    def execute(self, userdata):
        rospy.loginfo("Executing State CELLULAR_CONTROL")

        # save userdata so class can use it
        self.ud = userdata

        # clean nextstate variable if we are revisiting the state
        self.nextState = ""

        while not rospy.is_shutdown():

            # If stuff is initalized and telemetry connected change state
            self.mutex.acquire()

            if self.nextState != '':
                self.on_state_transition()
                return self.nextState

            self.mutex.release()

            self.rate.sleep()

    def on_state_transition(self):
        # unregister all subs and release all mutexes
        self.mutex.release()

    # CALLBACKS
    def heartbeat_lost(self, msg):
        self.mutex.acquire()

        # if we regain them, go to replanning and get new route based on current position
        if self.ud.telemStatus == TelemetryStatus.connected:
            self.nextState = 'replanning'

        self.mutex.release()

# TODO NEEDS COMPANION COMPUTER TO IMPLEMENT CELLULAR CONTROL FIRST

# TODO do a simple change_mode to hold position though api.
#  Do a simple mission_upload though api.
#  Do a simple start_mission though api

# Todo that should show that if cellular control is still active we can set hold_pos and upload mission to nearest
#  rallypoint or other landing spot
