// -*- mode: C++ -*-
/****************************************************************************
# kjMavLink Feather RFM95 firmware
# MavLink long range protocol for Adafruit Feather 32u4 RFM95 LoRa Radio 
# Copyright (c) 2017-2018, Kjeld Jensen <kjen@mmmi.sdu.dk> <kj@kjen.dk>
# SDU UAS Center, http://sdu.dk/uas 
# All rights reserved.
# 
# Licenced under GPL Version 2
# http://www.gnu.org/copyleft/gpl.html
#
# The included RadioHead library is Copyright (C) 2008 Mike McCauley
# https://github.com/adafruit/RadioHead
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#****************************************************************************
Revision
2017-11-09 KJ First version (based on AdaFruit RF95 TX/RX example.)
              Tested succesfully with AutoQuad flight controller and
              QGroundControl_AQ with AQ parameter COM_BAUD1 set to 57600.
2018-04-10 KJ Now supports mavlink_lora_lib(), ported to RFM69 board. 
2018-05-24 KJ New version ported back to RFM95 board. 
*****************************************************************************/
/* configuration */

/* #define DEBUG */
#define SERIAL_BAUD_RATE 57600 /* baud rate (not relevant for USB) */
#define RF95_FREQ 433.775 /* MHz */
#define RF95_TX_POWER 20 /* [5;23] dBm (corresponding to [3;200] mW) */

/****************************************************************************/
/* includes */
#include <SPI.h>
#include <RH_RF95.h>

extern "C"{
  #include "mavlink_lora_lib.h"
}

/****************************************************************************/
/* defines */
#define LED_ONBOARD 13

/* rf95 radio defines */
#define RFM95_CS 8 /* for feather32u4 */
#define RFM95_RST 4 /* for feather32u4 */
#define RFM95_INT 7 /* for feather32u4 */

RH_RF95 rf95(RFM95_CS, RFM95_INT); /* Singleton instance of the radio driver */
#define RADIO_RXBUF_SIZE 500
#define RADIO_RECV_MAX_LEN 256 /* limited by the unsigned char len of RadioHead recv function */
#define RADIO_RXBUF_SIZE_SAFE (RADIO_RXBUF_SIZE + RADIO_RECV_MAX_LEN)

/* serial defines */
#define SER_RXBUF_SIZE 500

/****************************************************************************/
/* global variables */
unsigned long now; /* time since boot in milli seconds */
unsigned short i, j;
char result;

/* led */
unsigned long led_tout;
unsigned char led_state;

/* serial */
unsigned char ser_rxbuf[SER_RXBUF_SIZE];
unsigned short ser_cnt;

/* rf95 radio */
uint8_t radio_rxbuf[RADIO_RXBUF_SIZE_SAFE]; /* being overly cautious on the length on to prevent a crash */
uint8_t radio_cnt;
short last_rssi;

/****************************************************************************/
/* setup function */
void setup()
{
  ml_init();
   
  pinMode(LED_ONBOARD, OUTPUT);

  Serial.begin(SERIAL_BAUD_RATE);
  Serial.setTimeout(0);
  
  while (!rf95.init())
  {
    while (1);
  }

/*
 * For maximum flexibility the user may decide on the spread spectrum modulation
 * bandwidth (BW), spreading factor (SF) and error correction rate (CR). (HopeRF p.9)
 * SF is defined in table HopeRF p.24, 7=128chips/symbol,9=512chips/symbol, 12=4096chips/symbol

Bw125Cr45Sf128   Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on. Default medium range.
Bw500Cr45Sf128   Bw = 500 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on. Fast+short range.
Bw31_25Cr48Sf512 Bw = 31.25 kHz, Cr = 4/8, Sf = 512chips/symbol, CRC on. Slow+long range.
Bw125Cr48Sf4096  Bw = 125 kHz, Cr = 4/8, Sf = 4096chips/symbol, CRC on. Slow+long range. 

http://www.airspayce.com/mikem/arduino/RadioHead/classRH__RF95.html#ab9605810c11c025758ea91b2813666e3
*/
  if (!rf95.setModemConfig(RH_RF95::Bw500Cr45Sf128))
  {
    while (1);
  }

  if (!rf95.setFrequency(RF95_FREQ))
  {
    while (1);
  }
  rf95.setTxPower(RF95_TX_POWER, false); 

}
/***************************************************************************/
void ml_parse_msg (unsigned char *msg)
{
  if (rf95.waitPacketSent(1) == true) /* timeout is in ms */
  {
    rf95.send (msg, (msg[1] + 8));  
    /* Serial.write (msg, msg[1] + 8); */
    /* rxbuf_filtered_cnt = 0; */
  }
}
/****************************************************************************/
/* main loop function */
void loop()
{ 
  /* housekeeping */
  now = millis();

  /* update LED */
  if (now > led_tout)
  {
    led_tout = now + 200;
    led_state = !led_state;
    digitalWrite(LED_ONBOARD, led_state);
  }

  /* check if data is available from the serial port */
  ser_cnt = Serial.available();

  if (ser_cnt > 0)
  {        
    /* reset buffer if buffer overflow detected */
    if (ser_cnt  > SER_RXBUF_SIZE)
    {
    }

    /* read and parse data */
    ser_cnt = Serial.readBytes(ser_rxbuf, ser_cnt);

    result = ml_rx_update(now, ser_rxbuf, ser_cnt);
  }

  /* check if data is available from the radio */
  if (rf95.available())
  {
    radio_cnt = RADIO_RXBUF_SIZE; /* maximum number of bytes to read */

    /* read data from radio */
    if (rf95.recv(radio_rxbuf, &radio_cnt))
    {
      /* send data to serial port */
      Serial.write (radio_rxbuf, radio_cnt);
    }
  }
}
/****************************************************************************/
