#!/usr/bin/env python

import rospy
from threading import Thread, Lock
from mavlink_lora.msg import mavlink_lora_heartbeat
from .CustomDefines import TelemetryStatus


class HeartbeatMonitor(Thread):

    def __init__(self, callback):
        # constructor
        Thread.__init__(self)

        # save callback
        self.callback = callback

        # vars
        self.heartbeat_last_received = rospy.get_time()
        self.heartbeat_timeout = 10
        self.mutex = Lock()
        self.telemStatus = TelemetryStatus.disconnected

        # set sleep rate. Check timing 5 times a second
        self.rate = rospy.Rate(5)

        # setup sub
        self.hb_sub = rospy.Subscriber("mavlink_heartbeat_rx", mavlink_lora_heartbeat, self.heartbeat_monitor_callback)

    def run(self):
        # run forever
        while not rospy.is_shutdown():

            self.mutex.acquire()

            # check for timeout, if more than timeout since last heartbeat, do callback
            if (rospy.get_time() - self.heartbeat_last_received) > self.heartbeat_timeout:
                # set telem as disconnected and do callback
                self.telemStatus = TelemetryStatus.disconnected
                self.callback(self.telemStatus)

            self.mutex.release()

            # spin
            rospy.spin()

            # sleep
            self.rate.sleep()

    def destory(self):
        self.hb_sub.unregister()
        self.mutex.release()

    # TODO might need to check on what id heartbeat comes from instead of assuming there is only one heartbeat
    def heartbeat_monitor_callback(self, msg):
        # save time
        rospy.loginfo("heartbeat_lost_debug")

        self.mutex.acquire()

        self.heartbeat_last_received = rospy.get_time()

        if self.telemStatus == TelemetryStatus.disconnected:
            self.telemStatus = TelemetryStatus.connected

        self.mutex.release()






