#!/usr/bin/env python

import paho.mqtt.client as mqtt
import smach
import time

from cc_fsm.CustomDefines import *
from cc_fsm.states import *


# set mqtt callbacks for reconnect. Subbing in this one
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc), flush=True)

    client.subscribe("heartbeat_rx", qos=1)
    client.subscribe("mission/upload/single_item", qos=1)
    client.subscribe("mission/upload/count", qos=1)
    client.subscribe("mission/upload/partial_write", qos=1)
    client.subscribe("mission/current", qos=1)
    client.subscribe("mission/ack", qos=2)
    client.subscribe("cmd/ack", qos=2)


def main():
    # create the smach statemachine
    sm = smach.StateMachine(outcomes=[])

    # variables for userdata
    sm.userdata.telemStatus = TelemetryStatus.disconnected
    sm.userdata.missionList = []
    sm.userdata.mqtt_client = mqtt.Client(client_id="cc_main", clean_session=True, transport="tcp", protocol=mqtt.MQTTv311)
    sm.userdata.mission_count = None
    sm.userdata.partial_mission_count = None

    # MQTT - Set subs and connect. We can sub to all topics here as we only use specific message_callback based on state
    sm.userdata.mqtt_client.on_connect = on_connect
    sm.userdata.mqtt_client.connect("localhost", 1883)

    # start background thread for network stuff
    sm.userdata.mqtt_client.loop_start()

    # sleep for a bit to setup stuff
    time.sleep(0.5)

    # open container
    with sm:
        # add states

        # BOOT
        smach.StateMachine.add('BOOT', Boot.Boot(),
                               transitions={'telemetry_connected': 'TELEMETRY_CONTROL'})
        # TELEMETRY CONTROL
        smach.StateMachine.add('TELEMETRY_CONTROL', TelemetryControl.TelemetryControl(),
                               transitions={'new_mission': 'NEW_MISSION',
                                            'append_mission': 'APPEND_MISSION',
                                            'cellular_control': 'CELLULAR_CONTROL',
                                            'telemetry_disconnected': 'TELEMETRY_DISCONNECTED'})

        # NEW MISSION
        smach.StateMachine.add('NEW_MISSION', NewMission.NewMission(),
                               transitions={'telemetry_control': 'TELEMETRY_CONTROL'})

        # NEW APPEND MISSION
        smach.StateMachine.add('APPEND_MISSION', AppendMission.AppendMission(),
                               transitions={'telemetry_control': 'TELEMETRY_CONTROL'})

        # RESET MISSION
        smach.StateMachine.add('RESET_MISSION', ResetMission.ResetMission(),
                               transitions={'telemetry_control': 'TELEMETRY_CONTROL'})

        # TELEMETRY DISCONNECTED
        smach.StateMachine.add('TELEMETRY_DISCONNECTED', TelemDisconnected.TelemDisconnected(),
                               transitions={'cellular_control': 'CELLULAR_CONTROL'})

        # CELLULAR CONTROL
        smach.StateMachine.add('CELLULAR_CONTROL', CellularControl.CellularControl(),
                               transitions={'reset_mission': 'RESET_MISSION'})

    # execute SMACH plan. Never expecting a outcome, as it should run indefinitely
    outcome = sm.execute()


if __name__ == '__main__':
    main()
