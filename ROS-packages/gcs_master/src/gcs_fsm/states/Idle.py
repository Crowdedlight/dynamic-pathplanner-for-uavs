#!/usr/bin/env python

import rospy
import smach
import threading
from ..CustomDefines import TelemetryStatus
from mavlink_lora.msg import mavlink_lora_heartbeat
from gcs_master.msg import goal_pos


# define state
class Idle(smach.State):

    # INIT
    def __init__(self):
        # init smach state
        smach.State.__init__(self,
                             outcomes=['pre-flight'],
                             input_keys=['telemStatus', 'goalPos'],
                             output_keys=['telemStatus', 'goalPos'])

        # Vars
        self.ud = None

        # workaround to change state from callback functions
        self.nextState = ""
        self.mutex = threading.Lock()
        self.rate = rospy.Rate(10)
        self.mission_newgoal_sub = None
        self.hb_sub = None

    def execute(self, userdata):
        rospy.loginfo("Executing State IDLE")

        # save userdata so class can use it. NEED TO DEFINE THIS BEFORE CALLBACKS
        self.ud = userdata

        # clean nextstate variable if we are revisiting the state
        self.nextState = ""

        # setup subs
        self.mission_newgoal_sub = rospy.Subscriber("gcs_master/mission/new_goal", goal_pos, self.new_goal_callback)
        # self.hb_sub = rospy.Subscriber("mavlink_heartbeat_rx", mavlink_lora_heartbeat, self.heartbeat_cb)

        while not rospy.is_shutdown():

            # If stuff is initalized and telemetry connected change state
            self.mutex.acquire()

            if self.nextState != '':
                self.on_state_transition()
                return self.nextState

            self.mutex.release()

            self.rate.sleep()

    def on_state_transition(self):
        # unregister all subs and release all mutexes
        # self.hb_sub.unregister()
        self.mission_newgoal_sub.unregister()
        self.mutex.release()

    # CALLBACKS

    def new_goal_callback(self, msg):
        self.mutex.acquire()
        # save new goal and change state
        pos = goal_pos()
        pos.lat = msg.lat
        pos.lon = msg.lon

        self.ud.goalPos = pos
        self.nextState = "pre-flight"

        self.mutex.release()
