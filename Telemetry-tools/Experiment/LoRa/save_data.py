
import serial
from datetime import datetime
import signal

def signal_handler(signal, frame):
    # self.stop_flag = True
    fout.close()
    print("graceful exit")
    exit()


# setup
device = "/dev/ttyACM0"
# device = "COM12"
baudrate = 57600 # or whatever you use

# install ctrl-c handler
signal.signal(signal.SIGINT, signal_handler)

# open serial
arduino_port = serial.Serial(device, baudrate=baudrate)

# open file
time_str = datetime.now().strftime("%Y%m%d_%H_%M_%S")
fout = open("results/" + time_str + '.csv', 'a')

# global vars
next_expected = 0
first_msg = True
num_messages = 0
num_missed_messages = 0

# keep logging until we decide to stop
while True:
    line = arduino_port.readline()
    line = line.decode('utf-8')

    # Output =>
    #           time since boot in ms => 35000 => 35sec
    #           rssi in dBm
    # print in console is fine as we can monitor time it has logged
    line_split = line.split(",")
    time_sec = int(line_split[0])
    msg = int(line_split[1])
    period = int(line_split[2])
    rssi = line_split[3]
    snr = line_split[4].rstrip()


    # if we missed a msg, don't flip what to expect as next package should be what we missed.
    if not first_msg and msg != next_expected:
        # missed packages is the distance from current msg to last received
        missed_amount = msg - next_expected
        num_missed_messages += missed_amount

    # set next expected msg
    next_expected = msg + 1

    # if first msg
    if first_msg:
        first_msg = False
        next_expected = msg + 1

    # print status so we can follow along
    print("Time: {0:.2f}s, MSG: {1}, MISSED_MSG: {2}, PERIOD: {3:.3f}s, RSSI: {4}, SNR: {5}".format(time_sec/1000, msg, num_missed_messages, period/1000, rssi, snr))

    # write to file
    fout.write("{0},{1},{2},{3},{4},{5}\n".format(time_sec, msg, num_missed_messages, period, rssi, snr))
    fout.flush()  # force it to append right away to ensure we save data on disk




