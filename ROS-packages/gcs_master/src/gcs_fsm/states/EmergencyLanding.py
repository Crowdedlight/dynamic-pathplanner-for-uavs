#!/usr/bin/env python

import rospy
import smach
import threading
from ..CustomDefines import *
from mavlink_lora.msg import mavlink_lora_heartbeat, mavlink_lora_command_land, mavlink_lora_pos


# define state
class EmergencyLanding(smach.State):

    # INIT
    def __init__(self):
        # init smach state
        smach.State.__init__(self,
                             input_keys=['telemStatus', 'curr_pos'],
                             output_keys=['telemStatus', 'curr_pos'])

        # Vars
        self.ud = None

        # workaround to change state from callback functions
        self.nextState = ""
        self.mutex = threading.Lock()
        self.emergency_land_sent = False
        self.curr_pos_received = False
        self.rate = rospy.Rate(10)
        self.cmd_ack_sub = None
        self.pos_sub = None

        # setup pubs
        self.cmd_land_pub = rospy.Publisher('mavlink_interface/command/land', mavlink_lora_command_land, queue_size=1)

    def execute(self, userdata):
        rospy.loginfo("Executing State EMERGENCY LANDING")

        # save userdata so class can use it
        self.ud = userdata

        # clean nextstate variable if we are revisiting the state
        self.nextState = ""

        # setup subs
        self.cmd_ack_sub = rospy.Subscriber("mavlink_interface/command/ack", mavlink_lora_heartbeat, self.command_ack_callback)
        self.pos_sub = rospy.Subscriber("mavlink_pos", mavlink_lora_pos, self.pos_callback)

        while not rospy.is_shutdown():

            # If stuff is initalized and telemetry connected change state
            self.mutex.acquire()

            # send land command if it hasn't been sent
            if not self.emergency_land_sent and self.curr_pos_received:
                self.send_emergency_land()

            if self.nextState != '':
                self.on_state_transition()
                return self.nextState

            self.mutex.release()

            self.rate.sleep()

    def send_emergency_land(self):
        # set sent to true
        self.emergency_land_sent = True

        # make msg
        msg = mavlink_lora_command_land()
        msg.lat = self.ud.curr_pos[0]
        msg.lon = self.ud.curr_pos[1]
        msg.altitude = self.ud.curr_pos[2]
        msg.precision_land_mode = PrecisionLandModes.PRECISION_LAND_MODE_DISABLED

        self.cmd_land_pub.publish(msg)

    def on_state_transition(self):
        # unregister all subs and release all mutexes
        self.cmd_ack_sub.unregister()
        self.pos_sub.unregister()
        self.mutex.release()

    # CALLBACKS
    def pos_callback(self, msg):
        self.mutex.acquire()

        # save current pos
        self.ud.curr_pos = [msg.lat, msg.lon, msg.alt]
        self.curr_pos_received = True

        self.mutex.release()

    def command_ack_callback(self, msg):
        self.mutex.acquire()

        # debug
        rospy.loginfo("Command: " + msg.command + ", result: " + msg.result_text)

        # if command is rejected, set send_status to zero so it tries again
        if msg.result != CommandResult.MAV_RESULT_ACCEPTED:
            # check what command it was and resend it
            if msg.command == MAVLINK_CMD_NAV_LAND:
                self.emergency_land_sent = False

        # else if mission accepted, move to next command or state
        elif msg.result == CommandResult.MAV_RESULT_ACCEPTED:
            pass
            # todo landing should happen now at pos. Maybe we need to disarm after landing? But it should do by itself

        self.mutex.release()
