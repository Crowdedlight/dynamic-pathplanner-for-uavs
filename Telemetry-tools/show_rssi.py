#!/usr/bin/env python


# imports
import rospy
from mavlink_lora.msg import mavlink_lora_radio_status
from datetime import datetime
import signal
from bokeh.plotting import figure, curdoc
from bokeh.models.sources import ColumnDataSource
from bokeh.client import push_session
from bokeh.models import Button
from functools import partial
from bokeh.layouts import column, row
from bokeh.server.server import Server

# parameters
mavlink_lora_radio_status_topic = '/mavlink_radio_status'


class rssi_node:
    def __init__(self):

        # launch node
        rospy.init_node('mavlink_lora_radio_status')

        # open file to log data.
        time_str = datetime.now().strftime("%Y%m%d_%H:%M:%S")
        self.fout = open("results/" + time_str + '.txt', 'a')
        self.time_offset = rospy.get_time()

        self.rate = rospy.Rate(10)
        self.doc = None

        # data holders
        self.data_x = []
        self.data_rssi_y = []
        self.data_remrssi_y = []
        self.data_noise_y = []
        self.data_remnoise_y = []

        # install ctrl-c handler
        signal.signal(signal.SIGINT, self.signal_handler)

        # setup liveplot
        self.fig_rssi = self.createFigure("RSSI")
        self.fig_remrssi = self.createFigure("Remote RSSI", self.fig_rssi.y_range)
        # self.fig_noise = self.createFigure("Noise")
        # self.fig_remnoise = self.createFigure("Remote Noise")

        # set data containers
        self.plot_rssi_ds = ColumnDataSource(data=dict(x=self.data_x, y=self.data_rssi_y))
        self.line_rssi = self.fig_rssi.line(x="x", y="y", color="firebrick", line_width=2, source=self.plot_rssi_ds)

        self.plot_remrssi_ds = ColumnDataSource(data=dict(x=self.data_x, y=self.data_remrssi_y))
        self.line_remrssi = self.fig_remrssi.line(x="x", y="y", color="deepskyblue", line_width=2, source=self.plot_remrssi_ds)

        # self.plot_noise_ds = ColumnDataSource(data=dict(x=self.data_x, y=self.data_noise_y))
        # self.line_noise = self.fig_noise.line(x="x", y="y", color="salmon", line_width=2, source=self.plot_noise_ds)
        #
        # self.plot_remnoise_ds = ColumnDataSource(data=dict(x=self.data_x, y=self.data_remnoise_y))
        # self.line_remnoise = self.fig_remnoise.line(x="x", y="y", color="lightblue", line_width=2, source=self.plot_remnoise_ds)

        # subs
        self.radio_msg_sub = rospy.Subscriber(mavlink_lora_radio_status_topic, mavlink_lora_radio_status, self.on_mavlink_msg)

        # wait until everything is running
        rospy.sleep(1)

        # start bokeh server
        self.server = Server({'/': self.addLayout}, num_procs=1)
        self.server.start()

        self.server.io_loop.add_callback(self.server.show, "/")
        self.server.io_loop.start()

    # define ctrl-c handler
    def signal_handler(self, signal, frame):
        # self.stop_flag = True
        self.radio_msg_sub.unregister()
        self.fout.close()
        print("graceful exit")
        exit()

    def createFigure(self, title, linked=None):
        fig = figure(plot_width=800,
                          plot_height=400,
                          x_axis_label='Seconds',
                          # x_axis_type='datetime',
                          x_axis_location='below',
                          # x_range=('2018-01-01', '2018-06-30'),
                          y_axis_label='dBm',
                          y_axis_type='linear',
                          y_axis_location='left',
                          # y_range=(0, 100),
                          title=title,
                          y_range=linked,
                          title_location='above',
                          toolbar_location='right'
                          # tools='save'
                          )

        fig.x_range.follow = "end"
        fig.x_range.follow_interval = 20
        fig.x_range.range_padding = 0

        return fig

    def plotUpdate(self):
        # do liveplot. Best practice to make new dict and update at once

        # RSSI
        new_data = dict()
        new_data['x'] = self.data_x
        new_data['y'] = self.data_rssi_y
        self.plot_rssi_ds.data = new_data

        # Remote RSSI
        new_data = dict()
        new_data['x'] = self.data_x
        new_data['y'] = self.data_remrssi_y
        self.plot_remrssi_ds.data = new_data

        # # Noise
        # new_data = dict()
        # new_data['x'] = self.data_x
        # new_data['y'] = self.data_noise_y
        # self.plot_noise_ds.data = new_data
        #
        # # Remote Noise
        # new_data = dict()
        # new_data['x'] = self.data_x
        # new_data['y'] = self.data_remnoise_y
        # self.plot_remnoise_ds.data = new_data

    def on_mavlink_msg(self, msg):
        # save data in file before doing live plot
        now = rospy.get_time()
        time = now - self.time_offset  # time in sec since start
        self.fout.write(
            "{0},{1},{2},{3},{4},{5},{6},{7}\n".format(time, msg.rssi, msg.remrssi, msg.noise, msg.remnoise, msg.txbuf,
                                                     msg.fixed, msg.rxerrors))
        # save data to array
        self.data_x.append(time)
        self.data_rssi_y.append(msg.rssi)
        self.data_remrssi_y.append(msg.remrssi)
        # self.data_noise_y.append(msg.noise)
        # self.data_remnoise_y.append(msg.noise)

        # schedule liveplot
        try:
            self.doc.add_next_tick_callback(partial(self.plotUpdate))
        except:
            pass

    def addLayout(self, doc):
        # set layout
        doc.add_root(column(row(self.fig_rssi, self.fig_remrssi))) #,
                            #row(self.fig_noise, self.fig_remnoise)))
        self.doc = doc


if __name__ == '__main__':
    rssi = rssi_node()
