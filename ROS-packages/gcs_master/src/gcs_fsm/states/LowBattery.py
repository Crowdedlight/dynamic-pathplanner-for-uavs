#!/usr/bin/env python

import rospy
import smach
import threading
from ..CustomDefines import *
from mavlink_lora.msg import mavlink_lora_heartbeat


# define state
class LowBattery(smach.State):

    # INIT
    def __init__(self):
        # init smach state
        smach.State.__init__(self,
                             outcomes=['mission_upload', 'emergency_stop'],
                             input_keys=['telemStatus', 'SOC'],
                             output_keys=['telemStatus', 'SOC'])

        # Vars
        self.ud = None

        # workaround to change state from callback functions
        self.nextState = ""
        self.mutex = threading.Lock()
        self.hb_sub = None

    def execute(self, userdata):
        rospy.loginfo("Executing State LOW BATTERY")

        # save userdata so class can use it
        self.ud = userdata

        # clean nextstate variable if we are revisiting the state
        self.nextState = ""

        # setup subs
        # self.hb_sub = rospy.Subscriber("mavlink_heartbeat_rx", mavlink_lora_heartbeat, self.heartbeat_callback)

        # make decision based on SOC, for now always to go emergency landing
        return 'emergency_stop'

    def on_state_transition(self):
        # unregister all subs and release all mutexes
        self.hb_sub.unregister()
        self.mutex.release()

    # CALLBACKS
    def heartbeat_callback(self, msg):
        # mark time now
        now = rospy.get_time()

# TODO make decision to go to rallypoint or landing here based on battery level and distance to rallypoint.
#  for now just land. possible Human-in-the-loop required.
