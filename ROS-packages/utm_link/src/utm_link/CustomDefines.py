#!/usr/bin/env python

from enum import Enum, IntEnum

# Enums
# connected == received heartbeat from GCS within timeout period
class TelemetryStatus(IntEnum):
    connected = 1
    disconnected = 2


# Defines
MAVLINK_MISSION_ITEM_INT_ID = 73
MAVLINK_CMD_MISSION_START_ID = 300
MAVLINK_CMD_SET_MODE_ID = 176
MAVLINK_CMD_ARM_ID = 400
MAVLINK_CMD_NAV_LAND = 21
MAVLINK_CMD_NAV_TAKEOFF = 22
MAVLINK_CMD_NAV_WAYPOINT = 16


class MavFrame(IntEnum):
    MAV_FRAME_GLOBAL = 0
    MAV_FRAME_LOCAL_NED = 1
    MAV_FRAME_MISSION = 2
    MAV_FRAME_GLOBAL_RELATIVE_ALT = 3
    MAV_FRAME_LOCAL_ENU = 4
    MAV_FRAME_GLOBAL_INT = 5
    MAV_FRAME_GLOBAL_RELATIVE_ALT_INT = 6


class GpsFix(IntEnum):
    GPS_FIX_TYPE_NO_GPS = 0     # No GPS connected
    GPS_FIX_TYPE_NO_FIX = 1     # No position information, GPS is connected
    GPS_FIX_TYPE_2D_FIX = 2     # 2D position
    GPS_FIX_TYPE_3D_FIX = 3     # 3D position
    GPS_FIX_TYPE_DGPS = 4       # DGPS/SBAS aided 3D position
    GPS_FIX_TYPE_RTK_FLOAT = 5  # RTK float, 3D position
    GPS_FIX_TYPE_RTK_FIXED = 6  # RTK Fixed, 3D position
    GPS_FIX_TYPE_STATIC = 7     # Static fixed, typically used for base stations
    GPS_FIX_TYPE_PPP = 8        # PPP, 3D position.


# ACK DEFINES
class MissionResult(IntEnum):
    MAV_MISSION_ACCEPTED = 0
    MAV_MISSION_ERROR = 1
    MAV_MISSION_UNSUPPORTED_FRAME = 2
    MAV_MISSION_UNSUPPORTED = 3
    MAV_MISSION_NO_SPACE = 4
    MAV_MISSION_INVALID = 5
    MAV_MISSION_INVALID_PARAM1 = 6
    MAV_MISSION_INVALID_PARAM2 = 7
    MAV_MISSION_INVALID_PARAM3 = 8
    MAV_MISSION_INVALID_PARAM4 = 9
    MAV_MISSION_INVALID_PARAM5_X = 10
    MAV_MISSION_INVALID_PARAM6_Y = 11
    MAV_MISSION_INVALID_PARAM7 = 12
    MAV_MISSION_INVALID_SEQUENCE = 13
    MAV_MISSION_DENIED = 14
    MAV_MISSION_OPERATION_CANCELLED = 15


class CommandResult(IntEnum):
    MAV_RESULT_ACCEPTED = 0
    MAV_RESULT_TEMPORARILY_REJECTED = 1
    MAV_RESULT_DENIED = 2
    MAV_RESULT_UNSUPPORTED = 3
    MAV_RESULT_FAILED = 4
    MAV_RESULT_IN_PROGRESS = 5


class PrecisionLandModes(IntEnum):
    PRECISION_LAND_MODE_DISABLED = 0        # Normal (non-precision) landing.
    PRECISION_LAND_MODE_OPPORTUNISTIC = 1   # Use precision landing if beacon detected when land command accepted, otherwise land normally.
    PRECISION_LAND_MODE_REQUIRED = 2        # Use precision landing, searching for beacon if not found when land command accepted (land normally if beacon cannot be found).


class BaseMode(IntEnum):
    MAV_MODE_FLAG_CUSTOM_MODE_ENABLED = 1


class PX4_CustomMainMode(IntEnum):
    PX4_CUSTOM_MAIN_MODE_MANUAL = 1
    PX4_CUSTOM_MAIN_MODE_ALTCTL = 2
    PX4_CUSTOM_MAIN_MODE_POSCTL = 3
    PX4_CUSTOM_MAIN_MODE_AUTO = 4
    PX4_CUSTOM_MAIN_MODE_ACRO = 5
    PX4_CUSTOM_MAIN_MODE_OFFBOARD = 6
    PX4_CUSTOM_MAIN_MODE_STABILIZED = 7
    PX4_CUSTOM_MAIN_MODE_RATTITUDE = 8
    PX4_CUSTOM_MAIN_MODE_SIMPLE = 9  # unused, but reserved for future use


class PX4_CustomSubMode(IntEnum):
    PX4_CUSTOM_SUB_MODE_AUTO_READY = 1,
    PX4_CUSTOM_SUB_MODE_AUTO_TAKEOFF = 2
    PX4_CUSTOM_SUB_MODE_AUTO_LOITER = 3
    PX4_CUSTOM_SUB_MODE_AUTO_MISSION = 4
    PX4_CUSTOM_SUB_MODE_AUTO_RTL = 5
    PX4_CUSTOM_SUB_MODE_AUTO_LAND = 6
    PX4_CUSTOM_SUB_MODE_AUTO_RTGS = 7
    PX4_CUSTOM_SUB_MODE_AUTO_FOLLOW_TARGET = 8
    PX4_CUSTOM_SUB_MODE_AUTO_PRECLAND = 9


def unsigned(n):
    return n & 0xFFFFFFFF


def decode_armed(base_mode):
    return bool(base_mode & 0x80)


def decode_custom_flightmode(custom_mode):
    # get bits for sub and main mode
    sub_mode = (custom_mode >> 24)
    main_mode = (custom_mode >> 16) & 0xFF

    # set default value to none
    main_mode_text = ""
    sub_mode_text = ""

    # make list of modes
    mainmodeList = ["MANUAL", "ALTITUDE_CONTROL", "POSITION_CONTROL", "AUTO", "ACRO", "OFFBOARD", "STABILIZED", "RATTITUDE"]
    submodeList = ["READY", "TAKEOFF", "LOITER", "MISSION", "RTL", "LAND", "RTGS", "FOLLOW_TARGET", "PRECLAND"]

    # get mode from list based on index and what mode we have active. -1 as values is 0-indexed
    # print(main_mode)
    main_mode_text = mainmodeList[main_mode - 1]
    sub_mode_text = submodeList[sub_mode - 1]

    # if no submode, just return mainmode
    if sub_mode_text == "":
        return main_mode_text
    else:
        return main_mode_text + " - " + sub_mode_text


# FAILSAFE DEFINES TODO for tests
BATTERY_MIN_FOR_TAKEOFF = 50