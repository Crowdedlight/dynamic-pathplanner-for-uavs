// -*- mode: C++ -*-

/* #define SERIAL  */
#define SERIAL_BAUD_RATE 57600 /* baud rate (not relevant for USB) */

/****************************************************************************/
/* includes */
#include <SPI.h>

extern "C"{
  #include "mavlink_lora_lib.h"
}

/****************************************************************************/
/* defines */
#define LED_ONBOARD 13

/* serial defines */
#define SER_RXBUF_SIZE 500

/****************************************************************************/
/* global variables */
unsigned long now; /* time since boot in milli seconds */
unsigned short i, j;
char result;
unsigned short numPackage = 0;

/* led */
unsigned long led_tout;
unsigned char led_state;

/* timeout */
unsigned long send_tout;

/****************************************************************************/
/* setup function */
void setup()
{
  ml_init();
   
  pinMode(LED_ONBOARD, OUTPUT);

  Serial.begin(SERIAL_BAUD_RATE);
  Serial.setTimeout(0);
}
/***************************************************************************/
void ml_parse_msg (unsigned char *msg)
{
}
void ml_tx_update (void) 
{
  Serial.write(txbuf, txbuf_cnt);
  txbuf_cnt = 0;
}
/****************************************************************************/
/* main loop function */
void loop()
{ 
  /* housekeeping */
  now = millis();

  /* update LED */
  if (now > led_tout)
  {
    led_tout = now + 200;
    led_state = !led_state;
    digitalWrite(LED_ONBOARD, led_state);
  }

  /* Send every 500ms */
  if (now > send_tout)
  {        
    send_tout = now + 500;
    
    //queue mission count as it has a short we can increment
    ml_queue_msg_mission_current(numPackage++);
    //ml_queue_msg_heartbeat(0, 0, 0, 0, 0, 0);
    ml_tx_update();
  }
}
/****************************************************************************/
