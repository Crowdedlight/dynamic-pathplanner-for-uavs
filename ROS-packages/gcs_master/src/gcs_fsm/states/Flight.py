#!/usr/bin/env python

import rospy
import smach
import threading
from ..CustomDefines import *
from ..HeartbeatMonitor import HeartbeatMonitor
from mavlink_lora.msg import mavlink_lora_mission_item_int, mavlink_lora_mission_list, mavlink_lora_status, mavlink_lora_heartbeat
from std_msgs.msg import UInt16, Empty


# define state
class Flight(smach.State):

    # INIT
    def __init__(self):
        # init smach state
        smach.State.__init__(self,
                             outcomes=['lost_datalink', 'mission_upload', 'emergency_stop', 'land', 'low_battery'],
                             input_keys=['telemStatus', 'missionList', 'SOC', 'flightmode', 'armed', 'new_mission_list'],
                             output_keys=['telemStatus', 'missionList', 'SOC', 'flightmode', 'armed', 'new_mission_list'])

        # Vars
        self.ud = None
        self.monitorThread = None
        self.mission_current_sub = None
        self.status_sub = None
        self.hb_sub = None
        self.battery_low_timestamp = None
        self.battery_threshold_period = 5 # battery has to be low for 5 sec before changing state

        # workaround to change state from callback functions
        self.nextState = ""
        self.mutex = threading.Lock()
        self.rate = rospy.Rate(10)

    def execute(self, userdata):
        rospy.loginfo("Executing State FLIGHT")

        # save userdata so class can use it
        self.ud = userdata

        # clean nextstate variable if we are revisiting the state
        self.nextState = ""

        # setup subs
        self.mission_current_sub = rospy.Subscriber("mavlink_interface/mission/current", UInt16, self.mission_current_callback)
        self.status_sub = rospy.Subscriber("mavlink_status", mavlink_lora_status, self.status_callback)
        self.hb_sub = rospy.Subscriber("mavlink_heartbeat_rx", mavlink_lora_heartbeat, self.heartbeat_callback)
        self.pathplanner_sub = rospy.Subscriber("pathplanner/path/new", mavlink_lora_mission_list, self.new_path_callback)
        self.emergency_stop_sub = rospy.Subscriber("gcs/emergency_stop", Empty, self.emergency_stop_callback)

        # todo setup heartbeat monitoring
        # self.monitorThread = HeartbeatMonitor(self.heartbeat_lost)
        # self.monitorThread.setName("Heartbeat Monitor")

        while not rospy.is_shutdown():

            # If stuff is initalized and telemetry connected change state
            self.mutex.acquire()

            # check if heartbeat is lost
            if self.ud.telemStatus == TelemetryStatus.disconnected:
                self.nextState = "lost_datalink"

            # check for battery low
            if self.battery_low_timestamp is not None and (rospy.get_time() - self.battery_low_timestamp) > self.battery_threshold_period:
                self.nextState = "low_battery"

            # check next state
            if self.nextState != '':
                self.on_state_transition()
                return self.nextState

            self.mutex.release()

            self.rate.sleep()

    def on_state_transition(self):
        # unregister all subs and release all mutexes
        self.hb_sub.unregister()
        self.mission_current_sub.unregister()
        self.status_sub.unregister()
        self.pathplanner_sub.unregister()

        self.mutex.release()

    # CALLBACKS
    def heartbeat_lost(self, status):
        self.mutex.acquire()

        # set new status, should update for both disconnected and connected
        self.ud.telemStatus = status

        self.mutex.release()

    def heartbeat_callback(self, msg):
        self.mutex.acquire()

        # update armed and flightmode status
        self.ud.armed = decode_armed(msg.base_mode)
        self.ud.flightmode = decode_custom_flightmode(msg.custom_mode)

        self.mutex.release()

    def status_callback(self, msg):
        self.mutex.acquire()

        # update battery level
        self.ud.SOC = msg.batt_remaining

        # mark timestamp if going below threshold
        if self.ud.SOC <= LOW_BATTERY and self.battery_low_timestamp is None:
            self.battery_low_timestamp = rospy.get_time()
        elif self.ud.SOC > LOW_BATTERY:
            self.battery_low_timestamp = None


        self.mutex.release()

    def mission_current_callback(self, msg):
        self.mutex.acquire()

        # check what item we are on
        missionItem = self.ud.missionList[msg.data]
        if missionItem.command == MAVLINK_CMD_NAV_LAND:
            self.nextState = "land"

        self.mutex.release()

    def new_path_callback(self, msg):
        # new path from path planner. Given as missionList
        self.mutex.acquire()
        # clear current list
        self.ud.new_mission_list = mavlink_lora_mission_list()
        self.ud.new_mission_list = msg

        # go to mission upload state
        self.nextState = "mission_upload"

        self.mutex.release()

    def emergency_stop_callback(self, msg):
        # got emergency stop in message. change state asap
        self.mutex.acquire()
        self.nextState = "emergency_stop"
        self.mutex.release()


# TODO MISSING
# register the heartbeat loss function and test that it actually works

# TODO DONE
# monitor battery. Has to be below for 5s => ideally 5 readings
# pathplanner outputs new route which we sub to and then upload them from here
# Monitor missino current. If seq gets to land item, change state to land


