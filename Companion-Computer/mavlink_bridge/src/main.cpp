/* system includes */

#include <cstdlib>
#include <cstdio>
#include <string>
#include <iostream>
#include <ctime>
#include <csignal>
#include <cerrno>
#include <fcntl.h>
#include <unistd.h>
#include <sched.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/timeb.h>
#include <cstring>

/***************************************************************************/
/* application includes */

#include "../include/mavlink_bridge.hpp"

/***************************************************************************/
/* global variables */

static sigset_t wait_mask;
mavlink_bridge mav_bridge;

/***************************************************************************/
void nullhandler(int signo)
{
}
/***************************************************************************/
/* handle CTRL-C gracefully */
static void quit (int signal) /* do not add void here */
{
    /* perform application cleanup */
    mav_bridge.shutdown();

    /* exit */
    exit (EXIT_SUCCESS);
}
/***************************************************************************/
static int sched_init (void)
{
    int err = false;
    struct sched_param my_sched_params;
    struct itimerval interval;

    printf("sched_init called\n");

    /* Setup high priority task */
    my_sched_params.sched_priority = sched_get_priority_max(SCHED_FIFO);
    int result = sched_setscheduler(0, SCHED_FIFO, &my_sched_params);
    if (! result)
    {
        printf("set scheduler success\n");

        /* lock all pages in memory */
        mlockall(MCL_CURRENT | MCL_FUTURE);

        /* implement the signal handler to handle CTRL-C gracefully */
        std::signal(SIGINT, quit);
        std::signal(SIGTERM, quit);

        /* set nullhandler on SIGALRM */
        std::signal(SIGALRM, nullhandler);

        /* setup timer for periodic signal */
        sigemptyset(&wait_mask);
        interval.it_value.tv_sec = 0;
        interval.it_interval.tv_sec = 0;
        interval.it_value.tv_usec = SCHED_INTERVAL;
        interval.it_interval.tv_usec = SCHED_INTERVAL;
        setitimer( ITIMER_REAL, &interval, NULL);
    }
    else {
        err = true;
        auto error = std::strerror(errno);
        std::cout << error << std::endl;
    }

    std::cout << "Return: " << std::to_string(err) << std::endl;
    return err;
}
/***************************************************************************/
int main (int argc, char **argv)
{
    printf("main started\n");

    /* initialize scheduler */
    if (! sched_init())
    {
        printf("sched_init success\n");

        /* initialize application */
        if (mav_bridge.init("/dev/PX4", 115200, "/dev/telemetry", 57600) == APP_INIT_OK)
        {
            printf("app init == APP_INIT_OK\n");

            int stop = false;
            while (! stop)
            {
                /* update application */
                mav_bridge.update();

                /* suspend until next event */
                sigsuspend(&wait_mask);
            }
        }
        quit(0);
    }

    return 0;
}
/***************************************************************************/
