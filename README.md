
# Dynamic pathplanner system for UAVs
This repo contains the work from my master thesis about getting a system running with a dynamic pathplanner on real drones. 
The system is built with ROS nodes and require a ground-station with telemtry to companion computer and a drone with PX4 FC. 

## Requirements
* ROS Melodic
* SMACH
* Python3
* C++
* MQTT Mosquitto
* MQTT Paho python & cpp bindings


## Install dependencies not done automatically though catkin
### Smach
Follow instructions [here](https://www.intorobotics.com/how-to-install-ros-kinetic-on-raspberry-pi-3-running-raspbian-stretch-lite/) to install melodic from source on rpi. Replace ``kinetic`` with ``melodic``.  
Before building with ``cmake_isolated`` install some stuff and clone the following repos into the src folder of the catkin workspace:
```
sudo apt-get install python-catkin-pkg python-catkin-pkg-modules
cd src/
git clone -b indigo-devel https://github.com/ros/executive_smach.git
git clone https://github.com/ros/rosbag_migration_rule.git
git clone -b indigo-devel https://github.com/ros/actionlib.git
git clone -b jade-devel https://github.com/ros/common_msgs.git
cd ..
```

### Mosquitto
**Ubuntu 18.04**
```
sudo apt-add-repository ppa:mosquitto-dev/mosquitto-ppa
sudo apt update
sudo apt-get install mosquitto
```

**Raspbian stretch**
```
cd
sudo wget http://repo.mosquitto.org/debian/mosquitto-repo.gpg.key
sudo apt-key add mosquitto-repo.gpg.key
cd /etc/apt/sources.list.d/
sudo wget http://repo.mosquitto.org/debian/mosquitto-stretch.list
sudo apt-get update
sudo apt-get install mosquitto mosquitto-clients
```

**Configuration of Broker**
```
pid_file /var/run/mosquitto.pid

# Do not keep messages, we don't want old messages we might have received before
persistence false
persistence_location /var/lib/mosquitto/
#persistent_client_expiration 2m

log_dest file /var/log/mosquitto/mosquitto.log
include_dir /etc/mosquitto/conf.d

log_type error
log_type warning
log_type notice
log_type information
#log_type debug

# If set to true, client connection and disconnection messages will be included
# in the log.
connection_messages true

# If set to true, add a timestamp value to each log message.
log_timestamp true
```

### Install Cmake 3.6

```
wget https://cmake.org/files/v3.7/cmake-3.7.2.tar.gz
tar -xvzf cmake-3.7.2.tar.gz
cd cmake-3.7.2
cmake .
make
sudo make install
```

### MQTT Paho
**Python:**  
```
pip3 install paho-mqtt
sudo apt-get install libmosquittopp-dev
```

**For Raspbian stretch**  
We need to install the libs for the root user, as supervisor run the program as root
```
sudo pip3 install paho-mqtt
sudo apt-get install libmosquittopp-dev
```


**C++**  
Run install script to install required mqtt library
```bash
sudo ./install_paho_library.sh
```

If using an IDE you might have to restart the IDE depending on when it sources the new linked libs

### Protobuf
The programs on the companion computer uses MQTT to communicate with eachother. To have common
defined messages protobuf is used to generate the messages in both c++ and python. 

To install protobuf run the script
```bash
sudo ./Companion-Computer/protocol_definition/install_protobuf.sh
```

The ``generate_msgs.sh`` script can be run to update and create the latest definitions for both
c++ and python programs. The c++ programs will automatically run this script when they get compiled

**For Raspbian Stretch**  
Library might not be needed with the above script. But the following packages are needed. Try to install these first
and see if it works, otherwise compile the other library. As compiling the protobuf from sources takes VERY LONG on
the pi. 
```
pip3 install protobuf
```
For supervisor script install it for root
```
sudo pip3 install protobuf
```

## Build mavlink_bridge on companion_computer
To build it for usage navigate to the ``mavlink_bridge`` folder. Then do following commands:
```
mkdir build; cd build
cmake ..
make
```

Then it should output a executable called ``mavlink_bridge`` that can be run. 

## UDEV rules for serial devices
To ensure that the programs on the CC always open the right ports udev rules are added for SiK radios, 
PX4 and Lora Radios. They are recognized by their vendorID and productID.
Add them as rules by opening/adding a file at ``/etc/udev/rules.d/10-local.rules`` and add the following changes:
```
# rules to always assign px4 and temeletry to same ports, even with different telemetry modules
# FC
SUBSYSTEM=="tty", ATTRS{idVendor}=="26ac", ATTRS{idProduct}=="0011", SYMLINK+="PX4"

# Lora Radio
SUBSYSTEM=="tty", ATTRS{idVendor}=="239a", ATTRS{idProduct}=="800c", SYMLINK+="telemetry"

# SiK Radio
SUBSYSTEM=="tty", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6015", SYMLINK+="telemetry"
``` 

Then apply the changes by ``sudo udevadm trigger``. 

From now on the ports should be setup so pixhawk goes to ``/dev/PX4`` and the telemetry radios goes to ``/dev/telemetry``

## Supervisor configuration for companion computer
The following supervisor configuration is used to keep the companion computer scripts running. First install 
supervisord with 
```
sudo apt-get install supervisor
```
Then add the following configs to it by adding them to a ``companioncomputer.conf`` file in ``/etc/supervisor/conf.d``
```
[program:mavlink_bridge]
process_name=%(program_name)s_%(process_num)02d
command=/home/pi/workspace/dynamic-pathplanner-for-uavs/Companion-Computer/mavlink_bridge/build/mavlink_bridge
autostart=true
autorestart=true
user=root
numprocs=1
redirect_stderr=true
stdout_logfile=/home/pi/workspace/logs/mavlink_bridge/bridge.log

[program:cc_main]
process_name=%(program_name)s_%(process_num)02d
command=/home/pi/workspace/dynamic-pathplanner-for-uavs/Companion-Computer/cc_main/run_with_supervisor.sh
autostart=true
autorestart=true
user=root
numprocs=1
redirect_stderr=true
stdout_logfile=/home/pi/workspace/logs/cc_main/main.log
```
Remember to add the folders:
```
mkdir -p /home/pi/workspace/logs/mavlink_bridge/
mkdir -p /home/pi/workspace/logs/cc_main
```
After adding the config start it by:
```
sudo supervisorctl reread
sudo supervisorctl update
sudo supervisorctl start mavlink_bridge:
sudo supervisorctl start cc_main:
```

## Configure OS to limit SD writes if not using UPS
To reduce the risk of SD card corruption with the frequent hard disconnects of power 
the OS is configured to reduce the amount of standard write operations that isn't 
important for the usecase. 

**Ideally you should use a UPS shield on the RPI like PiJuice to ensure soft shutdowns.**

Modify ``/etc/fstab`` to have the following:
```
none            /var/log        tmpfs   size=1M,noatime     0   0
/dev/mmcblk0p1  /boot           vfat    ro,noatime          0   2
/dev/mmcblk0p2  /               ext4    defaults,noatime    0   1
```
**Ensure that ``/dev/mmcblk0`` is the SD card, 
otherwise change it to what your SD card is named. If you change remember
to target the right partitions with p1 and p2** 

This will make the OS no write and save anything in ``/var/log``, make the boot partition
read-only and remove the writes to document when a file or item was last accessed. 
Last modified will still be saved. 

-------------------------

This readme will be expanded upon need. 