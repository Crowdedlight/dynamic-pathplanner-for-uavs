#!/usr/bin/env python


# imports
import rospy
from mavlink_lora.msg import mavlink_lora_radio_status
from std_msgs.msg import UInt16
from datetime import datetime
import signal
from bokeh.plotting import figure, curdoc
from bokeh.models.sources import ColumnDataSource
from bokeh.client import push_session
from bokeh.models import Button
from functools import partial
from bokeh.layouts import column, row
from bokeh.server.server import Server
from copy import deepcopy
from threading import Lock

# parameters
mavlink_lora_radio_status_topic = '/mavlink_radio_status'
mavlink_lora_mission_current_topic = '/mavlink_interface/mission/current'


class rssi_node:
    def __init__(self):

        # launch node
        rospy.init_node('mavlink_lora_radio_status')

        # open file to log data.
        time_str = datetime.now().strftime("%Y%m%d_%H_%M_%S")
        self.fout = open("results/" + time_str + '.csv', 'a')
        self.time_offset = rospy.get_time()
        self.last_received = rospy.get_time()

        self.rate = rospy.Rate(10)
        self.doc = None

        # data holders, populated with first element as init
        self.data_x = []
        self.data_rssi_y = []
        self.data_period_y = []
        self.data_rxerrors_y = []
        self.data_corrected_y = []
        self.data_missed_packages_y = []

        self.last_num_package = -1
        self.lost_packages = 0
        self.mutex = Lock()
        self.last_period = 0
        self.last_rssi = 0
        self.last_noise = 0
        self.last_remrssi = 0
        self.last_remnoise = 0
        self.last_txbuf = 0
        self.last_fixed = 0
        self.last_rxerrors = 0
        self.first_radio_status = True

        # install ctrl-c handler
        signal.signal(signal.SIGINT, self.signal_handler)

        # setup liveplot
        self.fig_rssi = self.createFigure("RSSI")
        self.fig_period = self.createFigure("Periode", y_axis_label="Count")
        self.fig_rxerrors = self.createFigure("Count of missed packets", y_axis_label="Count")
        self.fig_corrected = self.createFigure("Count of error corrected radio packets", y_axis_label="Count")

        # set data containers
        self.plot_rssi_ds = ColumnDataSource(data=dict(x=self.data_x, y=self.data_rssi_y))
        self.line_rssi = self.fig_rssi.line(x="x", y="y", color="firebrick", line_width=2, source=self.plot_rssi_ds)

        self.plot_period_ds = ColumnDataSource(data=dict(x=self.data_x, y=self.data_period_y))
        self.line_period = self.fig_period.line(x="x", y="y", color="deepskyblue", line_width=2, source=self.plot_period_ds)

        self.plot_rxerrors_ds = ColumnDataSource(data=dict(x=self.data_x, y=self.data_missed_packages_y))
        self.line_rxerrors = self.fig_rxerrors.line(x="x", y="y", color="salmon", line_width=2, source=self.plot_rxerrors_ds)

        self.plot_corrected_ds = ColumnDataSource(data=dict(x=self.data_x, y=self.data_corrected_y))
        self.line_corrected = self.fig_corrected.line(x="x", y="y", color="lightblue", line_width=2, source=self.plot_corrected_ds)

        # subs
        self.radio_msg_sub = rospy.Subscriber(mavlink_lora_radio_status_topic, mavlink_lora_radio_status, self.on_mavlink_msg)
        self.mission_count_msg_sub = rospy.Subscriber(mavlink_lora_mission_current_topic, UInt16, self.on_mission_current)

        # wait until everything is running
        rospy.sleep(1)

        # start bokeh server
        self.server = Server({'/': self.addLayout}, num_procs=1)
        self.server.start()

        self.server.io_loop.add_callback(self.server.show, "/")
        self.server.io_loop.start()

    # define ctrl-c handler
    def signal_handler(self, signal, frame):
        # self.stop_flag = True
        self.radio_msg_sub.unregister()
        self.fout.close()
        print("graceful exit")
        exit()

    def createFigure(self, title, linked=None, y_axis_label="dBm", x_axis_label="Seconds"):
        fig = figure(plot_width=800,
                          plot_height=400,
                          x_axis_label='Seconds',
                          # x_axis_type='datetime',
                          x_axis_location='below',
                          # x_range=('2018-01-01', '2018-06-30'),
                          y_axis_label='dBm',
                          y_axis_type='linear',
                          y_axis_location='left',
                          # y_range=(0, 100),
                          title=title,
                          y_range=linked,
                          title_location='above',
                          toolbar_location='right'
                          # tools='save'
                          )

        fig.x_range.follow = "end"
        fig.x_range.follow_interval = 20
        fig.x_range.range_padding = 0

        return fig

    def plotUpdate(self):
        # do liveplot. Best practice to make new dict and update at once

        # RSSI
        new_data = dict()
        new_data['x'] = self.data_x
        new_data['y'] = self.data_rssi_y
        self.plot_rssi_ds.data = new_data

        # Period
        new_data = dict()
        new_data['x'] = self.data_x
        new_data['y'] = self.data_period_y
        self.plot_period_ds.data = new_data

        # RX errors
        new_data = dict()
        new_data['x'] = self.data_x
        new_data['y'] = self.data_missed_packages_y
        self.plot_rxerrors_ds.data = new_data

        # Corrected radio packets
        new_data = dict()
        new_data['x'] = self.data_x
        new_data['y'] = self.data_corrected_y
        self.plot_corrected_ds.data = new_data

    def on_mission_current(self, msg):

        if self.first_radio_status:
            # waiting on getting first telemetry status before starting to save data.
            rospy.loginfo("waiting on first telemetry msg")
            return

        self.mutex.acquire()

        now = rospy.get_time()
        self.last_period = now - self.last_received

        # check if we have missed packages
        if self.last_num_package != -1 and self.last_num_package + 1 == msg.data:
            # got next message as expected
            self.last_num_package = msg.data

        elif self.last_num_package == -1:
            # first message, just set last msg
            self.last_num_package = msg.data
        else:
            # messages lost, find out how many
            self.lost_packages += msg.data - (self.last_num_package - 1)
            self.last_num_package = msg.data

        # save data in file before doing live plot
        time = now - self.time_offset  # time in sec since start

        self.fout.write(
            "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}\n".format(time, msg.data, self.last_period, self.last_rssi, self.last_noise,
                                                               self.last_remrssi, self.last_remnoise, self.last_txbuf,
                                                               self.last_fixed, self.last_rxerrors, self.lost_packages))

        rospy.loginfo(
            "TIME: {0:.2f}, PACKET: {1}, PERIOD: {2:.3f}, RSSI: {3}, Lost Packets: {4}\n".format(time, msg.data, self.last_period, self.last_rssi, self.lost_packages))
        # save data to array
        self.data_x.append(time)
        self.data_rssi_y.append(self.last_rssi)
        self.data_period_y.append(self.last_period)
        self.data_corrected_y.append(self.last_fixed)
        self.data_missed_packages_y.append(self.lost_packages)

        self.mutex.release()

        # schedule liveplot
        try:
            self.doc.add_next_tick_callback(partial(self.plotUpdate))
        except:
            pass

        self.last_received = now

    def on_mavlink_msg(self, msg):

        # rospy.loginfo("Radio status msg received")

        self.mutex.acquire()

        if self.first_radio_status:
            self.first_radio_status = False

        self.last_rssi = msg.rssi
        self.last_noise = msg.noise
        self.last_remrssi = msg.remrssi
        self.last_remnoise = msg.remnoise
        self.last_txbuf = msg.txbuf
        self.last_fixed = msg.fixed
        self.last_rxerrors = msg.rxerrors

        self.mutex.release()


    def addLayout(self, doc):
        # set layout
        doc.add_root(column(row(self.fig_rssi, self.fig_period),
                            row(self.fig_rxerrors, self.fig_corrected)))
        self.doc = doc


if __name__ == '__main__':
    rssi = rssi_node()
