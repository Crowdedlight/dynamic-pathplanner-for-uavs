#!/usr/bin/env python

import rospy
import smach
import threading
from ..CustomDefines import *
from mavlink_lora.msg import mavlink_lora_heartbeat, mavlink_lora_command_set_mode, mavlink_lora_command_ack


# define state
class LostDatalink(smach.State):

    # INIT
    def __init__(self):
        # init smach state
        smach.State.__init__(self,
                             outcomes=['cellular_control'],
                             input_keys=['telemStatus'],
                             output_keys=['telemStatus'])

        # Vars
        self.ud = None

        # workaround to change state from callback functions
        self.nextState = ""
        self.mutex = threading.Lock()

        # setup pubs
        self.setMode_pub = rospy.Publisher('mavlink_interface/command/set_mode', mavlink_lora_command_set_mode, queue_size=1)

    def execute(self, userdata):
        rospy.loginfo("Executing State LOST_DATALINK")

        # save userdata so class can use it
        self.ud = userdata

        # clean nextstate variable if we are revisiting the state
        self.nextState = ""

        # TODO this state exists as an option to otherwise alert operator that connection is lost. Currently there is no
        #  alerting, but the state makes it easy to add that functionality as needed

        # as we have lost data_link it is not likely we can send a hold command from here, and the companion_computer
        # should have done that for us. Do attempt to send one anyway for good measure. Then change to Cellular_control
        # In cellular control the flightmode should be set as hold anyway.
        self.setmode_hold()

        # next state
        return "cellular_control"

    def on_state_transition(self):
        # unregister all subs and release all mutexes
        # self.hb_sub.unregister()
        self.mutex.release()

    def setmode_hold(self):
        # make msg
        msg = mavlink_lora_command_set_mode()
        msg.custom_mode = PX4_CustomMainMode.HOLD_MODE

        # send message
        self.setMode_pub.publish(msg)
