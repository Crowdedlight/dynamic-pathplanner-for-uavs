#!/usr/bin/env python

import threading
import time
from datetime import datetime

import smach

from cc_fsm.CustomDefines import *
import cc_fsm.protocol.cc_messages_pb2 as messages


# define state
class Boot(smach.State):

    # INIT
    def __init__(self):
        # init smach state
        smach.State.__init__(self,
                             outcomes=['telemetry_connected'],
                             input_keys=['telemStatus', 'SOC', 'flightmode', 'armed', 'mqtt_client'],
                             output_keys=['telemStatus', 'SOC', 'flightmode', 'armed', 'mqtt_client'])

        # Vars
        self.heartbeat_last_received = 0
        self.last_heartbeats_periods = []
        self.ud = None

        # workaround to change state from callback functions
        self.nextState = ""
        self.mutex = threading.Lock()

        # setup subs
        self.client = None

    def on_state_transition(self):
        self.mutex.release()
        # release mqtt callback?
        self.client.message_callback_remove("heartbeat_rx")

    def execute(self, userdata):
        print("Executing State BOOT", flush=True)

        # save userdata so class can use it?
        self.ud = userdata
        self.client = self.ud.mqtt_client

        # clean nextstate variable if we are revisiting the state
        self.nextState = ""

        # register mqtt callback
        self.client.message_callback_add("heartbeat_rx", self.heartbeat_callback)

        while True:
            # check if we should change state
            self.mutex.acquire()

            if self.nextState != "":
                self.on_state_transition()
                return self.nextState
            self.mutex.release()

            # sleep for 100ms
            time.sleep(0.1)

    # CALLBACKS
    def heartbeat_callback(self, client, userdata, msg):
        # mark time now
        now = datetime.now()

        # parse msg
        hb = messages.heartbeat()
        hb.ParseFromString(msg.payload)

        # print("Topic :{0} Payload :{1}".format(msg.topic, msg))

        # if first time mark and exit
        if self.heartbeat_last_received == 0:
            self.heartbeat_last_received = now
            return

        # calculate period since last heartbeat
        period = now - self.heartbeat_last_received

        # add to heartbeat list
        self.last_heartbeats_periods.append(period)

        # remove first from array if more than 4
        if len(self.last_heartbeats_periods) >= 4:
            self.last_heartbeats_periods.pop(0)

        # if length is 4 calculate average period
        heartbeat_avg_period = 5000
        if len(self.last_heartbeats_periods) >= 3:
            heartbeat_avg_period = sum(self.last_heartbeats_periods) / len(self.last_heartbeats_periods)
            # debug
            print("len: {}, avg period: {}".format(len(self.last_heartbeats_periods), heartbeat_avg_period), flush=True)

        # check if avg period is <= 2 then we got stable connection
        if heartbeat_avg_period <= 2:
            # set status as connected and change state
            self.mutex.acquire()
            self.ud.telemStatus = TelemetryStatus.connected
            self.nextState = "telemetry_connected"
            self.mutex.release()

        # last received heartbeat
        self.heartbeat_last_received = datetime.now()

        # save data of armed and flightmode
        self.ud.armed = decode_armed(hb.base_mode)
        self.ud.flightmode = decode_custom_flightmode(hb.custom_mode)


