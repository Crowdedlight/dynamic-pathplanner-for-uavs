#!/usr/bin/env python

import rospy
import smach
import threading
from ..CustomDefines import *
from mavlink_lora.msg import mavlink_lora_command_ack, mavlink_lora_command_set_mode


# define state
class EmergencyStop(smach.State):

    # INIT
    def __init__(self):
        # init smach state
        smach.State.__init__(self,
                             outcomes=['mission_upload', 'emergency_landing'],
                             input_keys=['telemStatus'],
                             output_keys=['telemStatus'])

        # Vars
        self.ud = None

        # workaround to change state from callback functions
        self.nextState = ""
        self.mutex = threading.Lock()
        self.set_mode_sent = False
        self.rate = rospy.Rate(10)
        self.cmd_ack_sub = None

        # setup pubs
        self.setMode_pub = rospy.Publisher('mavlink_interface/command/set_mode', mavlink_lora_command_set_mode, queue_size=1)

    def execute(self, userdata):
        rospy.loginfo("Executing State EMERGENCY STOP")

        # save userdata so class can use it
        self.ud = userdata

        # clean nextstate variable if we are revisiting the state
        self.nextState = ""

        # setup subs
        self.cmd_ack_sub = rospy.Subscriber("mavlink_interface/command/ack", mavlink_lora_command_ack, self.command_ack)

        while not rospy.is_shutdown():

            # If stuff is initalized and telemetry connected change state
            self.mutex.acquire()

            # if not sent, then send request for hold mode
            if not self.set_mode_sent:
                self.send_holdmode()

            if self.nextState != '':
                self.on_state_transition()
                return self.nextState

            self.mutex.release()

            self.rate.sleep()

    def on_state_transition(self):
        # unregister all subs and release all mutexes
        self.cmd_ack_sub.unregister()
        self.mutex.release()

    def send_holdmode(self):
        self.set_mode_sent = True

        # make msg
        msg = mavlink_lora_command_set_mode()

        msg.mode = BaseMode.MAV_MODE_FLAG_CUSTOM_MODE_ENABLED
        msg.custom_mode = PX4_CustomMainMode.PX4_CUSTOM_MAIN_MODE_AUTO
        msg.custom_sub_mode = PX4_CustomSubMode.PX4_CUSTOM_SUB_MODE_AUTO_LOITER

        self.setMode_pub.publish(msg)

    # CALLBACKS
    def command_ack(self, msg):
        self.mutex.acquire()

        # debug
        rospy.loginfo("Command: " + msg.command + ", result: " + msg.result_text)

        # if command is rejected, set send_status to zero so it tries again
        if msg.result != CommandResult.MAV_RESULT_ACCEPTED:
            # check what command it was and resend it
            if msg.command == MAVLINK_CMD_SET_MODE_ID:
                self.set_mode_sent = False

        # else if mission accepted, move to next command or state
        elif msg.result == CommandResult.MAV_RESULT_ACCEPTED:
            # if setmode, next state
            if msg.command == MAVLINK_CMD_SET_MODE_ID:
                # todo Human in the loop here... For simplicity we just go to emergency landing for now
                self.nextState = 'emergency_landing'

        self.mutex.release()
