#!/usr/bin/env python

import rospy
import smach
import threading
from copy import deepcopy
from ..CustomDefines import TelemetryStatus
from mavlink_lora.msg import mavlink_lora_mission_ack, mavlink_lora_mission_list


# define state
class MissionUpload(smach.State):

    # INIT
    def __init__(self):
        # init smach state
        smach.State.__init__(self,
                             outcomes=['emergency_stop', 'flight'],
                             input_keys=['new_mission_list', 'missionList'],
                             output_keys=['new_mission_list', 'missionList'])

        # Vars
        self.ud = None

        # workaround to change state from callback functions
        self.nextState = ""
        self.mutex = threading.Lock()
        self.rate = rospy.Rate(10)
        self.hb_sub = None
        self.retries = 0
        self.max_retries = 3

        self.upload_mission_pub = rospy.Publisher('mavlink_interface/mission/mavlink_upload_mission', mavlink_lora_mission_list, queue_size=1)
        self.upload_append_mission_pub = rospy.Publisher('mavlink_interface/mission/append_mission', mavlink_lora_mission_list, queue_size=1)

    def execute(self, userdata):
        rospy.loginfo("Executing State MISSION UPLOAD")

        # save userdata so class can use it
        self.ud = userdata

        # clean nextstate variable if we are revisiting the state
        self.nextState = ""

        # vars
        self.retries = 0
        self.max_retries = 3

        # setup subs
        self.mission_ack_sub = rospy.Subscriber("mavlink_interface/mission/ack", mavlink_lora_mission_ack, self.mission_ack_callback)

        # send first attempt to upload to FC
        if self.ud.new_mission_list.append_start_index is not None:
            self.send_append_mission_to_fc()
        else:
            self.send_mission_to_fc()

        while not rospy.is_shutdown():
            # If stuff is initalized and telemetry connected change state
            self.mutex.acquire()

            if self.nextState != '':
                self.on_state_transition()
                return self.nextState

            self.mutex.release()

            self.rate.sleep()

    def on_state_transition(self):
        # unregister all subs and release all mutexes
        self.mission_ack_sub.unregister()
        self.ud.new_mission_list = mavlink_lora_mission_list()

        self.mutex.release()

    def send_mission_to_fc(self):
        # increment retries
        self.retries += 1

        self.upload_mission_pub.publish(self.ud.new_mission_list)

    def send_append_mission_to_fc(self):
        # increment retries
        self.retries += 1

        self.upload_append_mission_pub.publish(self.ud.new_mission_list)

    # CALLBACKS
    def mission_ack_callback(self, msg):
        self.mutex.acquire()

        # check if ack is 0, success, otherwise retry until max retries then fail and go to emergency state
        if msg.result == 0:
            # success
            rospy.loginfo("Mission uploaded successfully")
            self.ud.missionList = deepcopy(self.ud.new_mission_list.waypoints)
            self.nextState = "flight"
        else:
            # failed in some way. Check if retrying
            rospy.loginfo("Mission upload {} failed".format(self.retries))

            if self.retries > self.max_retries:
                rospy.loginfo("Max retries in uploading mission. Calling an emergency")
                self.nextState = "emergency_stop"
            else:
                # retry
                rospy.loginfo("Retrying mission upload")
                self.mutex.release()
                self.send_mission_to_fc()
                return

        self.mutex.release()


