#!/bin/sh
#
# Generate the headers and place them directly in the correct folders
# Has to give it input file manually. So edit here and add more files if more files are needed
#
protoc --proto_path=src --cpp_out=../mavlink_bridge/protocol --python_out=../cc_main/cc_fsm/protocol src/cc_messages.proto
