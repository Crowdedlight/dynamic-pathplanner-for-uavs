#!/usr/bin/env python

import rospy
import smach
import threading
from ..CustomDefines import *
from mavlink_lora.msg import mavlink_lora_heartbeat, mavlink_lora_mission_list, mavlink_lora_mission_item_int, mavlink_lora_pos, mavlink_lora_status, mavlink_lora_gps_raw, mavlink_lora_mission_ack
from std_msgs.msg import String
from gcs_master.msg import goal_pos

# define state
class Preflight(smach.State):

    # INIT
    def __init__(self):
        # init smach state
        smach.State.__init__(self,
                             outcomes=['preflight_failed', 'takeoff'],
                             input_keys=['goalPos', 'missionList', 'SOC', 'gps_fix'],
                             output_keys=['goalPos', 'missionList', 'SOC', 'gps_fix'])

        # Vars
        self.ud = None
        self.preflight_checks_done = False
        self.missionList = None

        # workaround to change state from callback functions
        self.nextState = ""
        self.mutex = threading.Lock()
        self.rate = rospy.Rate(10)

        # subs
        self.pos_sub = None
        self.gpsraw_sub = None
        self.status_sub = None
        self.mission_list_sub = None
        self.mission_ack_sub = None

        # setup pubs
        # TODO replace type with custom message from pathplanner. Something that have curr_pos and goal_pos
        self.newMission_pub = rospy.Publisher('pathplanner/mission/new', goal_pos, queue_size=1)
        self.uploadMission_pub = rospy.Publisher('mavlink_interface/mission/mavlink_upload_mission', mavlink_lora_mission_list, queue_size=1)

    def execute(self, userdata):
        rospy.loginfo("Executing State Preflight")

        # save userdata so class can use it
        self.ud = userdata

        # reset vars
        self.preflight_checks_done = False

        # clean nextstate variable if we are revisiting the state
        self.nextState = ""

        # setup subs
        self.mission_ack_sub = rospy.Subscriber("mavlink_interface/mission/ack", mavlink_lora_mission_ack, self.mission_ack_callback)
        self.mission_list_sub = rospy.Subscriber("pathplanner/mission/list", mavlink_lora_mission_list, self.mission_list_callback)
        self.status_sub = rospy.Subscriber("mavlink_status", mavlink_lora_status, self.status_callback)
        self. gpsraw_sub = rospy.Subscriber("mavlink_gps_raw", mavlink_lora_gps_raw, self.gps_raw_callback)
        self.pos_sub = rospy.Subscriber("mavlink_gps_pos", mavlink_lora_pos, self.pos_callback)

        # send goal and current pos to pathplanner. won't actually send it before we got GPS fix
        self.send_goal_pathplanner()

        # preflight check
        self.preflight_check()

        # then wait for pathplanner to publish mission_plan on topic
        # todo consider adding so pathplanner can tell if waiting for clearing goal/start and just waits

        while not rospy.is_shutdown():
            # If stuff is initalized and telemetry connected change state
            self.mutex.acquire()

            if self.nextState != '':
                self.on_state_transition()
                return self.nextState

            self.mutex.release()

            # sleep
            self.rate.sleep()

    def on_state_transition(self):
        # unregister all subs and release all mutexes
        self.mission_ack_sub.unregister()
        self.mission_list_sub.unregister()
        self.status_sub.unregister()
        self.gpsraw_sub.unregister()
        self.pos_sub.unregister()
        self.mutex.release()

    def send_goal_pathplanner(self, event=None):
        # check if gpsfix is aquired, otherwise wait
        self.mutex.acquire()
        if self.ud.gps_fix >= 0: #GpsFix.GPS_FIX_TYPE_3D_FIX:
            # create object and send current pos + destination TODO
            self.newMission_pub.publish(self.ud.goalPos)

        else:
            # requeue to check for gps fix
            rospy.Timer(rospy.Duration(1), self.send_goal_pathplanner, oneshot=True)

        self.mutex.release()

    # PREFLIGHT CHECKS
    def preflight_check(self, event=None):
        self.mutex.acquire()

        # check levels, accepts 3D fix or better https://mavlink.io/en/messages/common.html#GPS_FIX_TYPE
        if self.ud.SOC >= BATTERY_MIN_FOR_TAKEOFF and self.ud.gps_fix >=  0: #GpsFix.GPS_FIX_TYPE_3D_FIX:
            self.preflight_checks_done = True
            rospy.loginfo("Preflight-checks passed. SOC: " + str(self.ud.SOC) + ", GPS_FIX: " + str(self.ud.gps_fix))

        # check if battery hasn't been read or is too low
        elif self.ud.SOC != -1 and self.ud.SOC < BATTERY_MIN_FOR_TAKEOFF:
            # preflight check failed, battery too low
            rospy.loginfo("Battery Level too low. SOC at: " + str(self.ud.SOC))
            self.nextState = "preflight_failed"

        else:
            # wait 2sec and recheck
            rospy.Timer(rospy.Duration(2), self.preflight_check, oneshot=True)
            rospy.loginfo("Preflight_check not completed...waiting 2sec")

        self.mutex.release()

    # CALLBACKS
    def status_callback(self, msg):
        # save gps_fix and battery level
        self.mutex.acquire()
        self.ud.SOC = msg.batt_remaining
        self.mutex.release()

    def gps_raw_callback(self, msg):
        # save gps fix
        self.mutex.acquire()
        self.ud.gps_fix = msg.fix_type
        self.mutex.release()

    def pos_callback(self, msg):
        # save current position in userdata
        self.mutex.acquire()
        self.ud.current_position = (msg.lat, msg.lon)  # TODO consider if using object or something instead?
        self.mutex.release()

    def mission_ack_callback(self, msg):
        # if ack is success go to TAKEOFF
        self.mutex.acquire()

        if msg.result == MissionResult.MAV_MISSION_ACCEPTED:
            self.nextState = "takeoff"
            # save waypoints list in userdata
            self.ud.missionList = self.missionList.waypoints
        else:
            rospy.loginfo("FAILED MISSION UPLOAD" + msg.result_text)
            # go to idle as we failed execution of mission
            self.nextState = "preflight_failed"

        self.mutex.release()

    def mission_list_callback(self, msg):
        # check if preflight checks are done and then upload mission.
        self.mutex.acquire()

        rospy.loginfo("Received mission list from pathplanner")

        # save mission_msg
        self.missionList = msg

        self.mutex.release()

        # start check if preflights are done and upload mission
        self.mission_upload_callback()

    def mission_upload_callback(self, event=None):
        self.mutex.acquire()

        if self.preflight_checks_done:
            self.uploadMission_pub.publish(self.missionList)
        else:
            # wait 2sec and check again
            rospy.Timer(rospy.Duration(2), self.mission_upload_callback, oneshot=True)

        self.mutex.release()

