#!/usr/bin/env python

import threading
import rospy
from .CustomDefines import TelemetryStatus

class utm_link:
    def __init__(self):
        rospy.init_node('utm_link')

        # vars
        self.drone_lat = None
        self.drone_lon = None
        self.drone_alt = None

        self.utm_missionlist = []
        self.current_wp = None

        # subs

        # pubs

    def run(self):

        # forever just spin loop...
        rospy.spin()

    # CALLBACKS
    def current_waypoint(self, msg):
        pass

    # Position
    def current_position(self, msg):
        pass

    # Start transmitting
    def start_transmitting(self, msg):
        pass
        # todo should probably not upload anything to UTM until we received a mission. So until we are in preflight
        #  do not upload to utm. When we are in preflight upload until we are at post-flight.
        # todo could be a service like current_mission? So we keep asking, instead of expecting to be told?

    # ASKING

    # ask for current mission every 10sec. Ensures that even if missing a mission upload the newest mission will
    #  be retrieved within 10sec. Also ensures we always have the mission that successfully has been uploaded, not one
    #  attempted to be uploaded.
    def ask_current_mission(self):
        pass
        # todo service call to get current mission from gcs_master?
        #  then comparing current mission to the one that is uploaded to UTM
        #  if not identical upload new mission

