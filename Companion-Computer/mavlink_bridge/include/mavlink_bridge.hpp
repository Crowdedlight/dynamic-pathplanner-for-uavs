//
// Created by Crow on 25-03-2019.
//

#ifndef MAVLINK_BRIDGE_MAVLINK_BRIDGE_HPP
#define MAVLINK_BRIDGE_MAVLINK_BRIDGE_HPP

#include <utility>
#include <stdlib.h>
#include <algorithm>    // std::max
#include <vector>
#include <string>
#include <chrono>
#include <sys/time.h>
#include <iostream>
#include <functional>

#include "mavlink_lora_lib.hpp"
#include "cpptime.h"

#include "mqtt_callback.hpp"
#include <mqtt/async_client.h>

#include "../protocol/cc_messages.pb.h"


using namespace std;
using namespace std::chrono;

/* Defines */
#define true					1
#define false					0
#define SCHED_INTERVAL 			5e3; /* 200 Hz */
#define APP_INIT_OK				0

#define DEFAULT_TIMEOUT_TIME 1500 //1.5sec
#define MISSION_ITEM_TIMEOUT_TIME 1500 //1.5sec
#define MISSION_ITEM_SINGLE_TIMEOUT_TIME 5000 //5sec
#define MISSION_MAX_RETRIES 5
#define HEARTBEAT_RATE 0.5 //2 //hz

class mavlink_bridge : callback {

public:
    mavlink_bridge();

    /// program workers
    int init(string serial_fc, int baud_fc, string serial_gcs, int baud_gcs);
    void update();
    void shutdown();

private:
    /// Lora Lib
    mavlink_lora_lib lora_lib_FC; // to and from FC
    mavlink_lora_lib lora_lib_GCS; // to and from GCS

    void parse_msg_from_fc(unsigned char *msg);
    void parse_msg_from_gcs(unsigned char *msg);

    unsigned long update_cnt;

    /// MQTT
    const string SERVER_ADDRESS	= "tcp://localhost:1883";
    const string CLIENT_ID		= "mavlink_bridge";
    const int QOS_least_once = 1; //1
    const int QOS_excatly_once = 2; //1
    const int N_RETRY_ATTEMPTS = 5;

    vector<string> sub_topics_QOS1;
    vector<string> sub_topics_QOS2;

    // pointers to client
    mqtt::async_client_ptr mqtt_client;
    mqtt::connect_options connOpts;
    int nretry = 50;

    /// MQTT Callback override functions
    void reconnect() override;
    void on_failure(const mqtt::token &tok) override;
    void connected(const string& cause) override;
    void connection_lost(const string &cause) override;
    void message_arrived(mqtt::const_message_ptr msg) override;

    /// Publisher pointers
    mqtt::topic_ptr mission_item_single_pub;
    mqtt::topic_ptr mission_ack_pub;
    mqtt::topic_ptr heartbeat_rx_pub;
    mqtt::topic_ptr cmd_ack_pub;
    mqtt::topic_ptr mission_current_pub;
    mqtt::topic_ptr mission_count_pub;
    mqtt::topic_ptr mission_partial_write_pub;

    /// System Status vars
    uint16_t sys_status_voltage_battery;
    int8_t sys_status_battery_remaining;
    uint16_t sys_status_cpu_load;
    unsigned long secs_init;
    chrono::time_point<chrono::system_clock, chrono::nanoseconds> last_heard = chrono::system_clock::now();
    chrono::time_point<chrono::system_clock, chrono::nanoseconds> last_heard_sys = chrono::system_clock::now();

    /// Mission upload operations vars
    unsigned int mission_up_count = 0; /*< Total count of mission elements to be uploaded*/
    int mission_up_index = -1; /*< Current mission item getting uploaded */
    vector<mavlink_mission_item_int_t> missionlist; /*< list of all waypoints for upload */
    bool mission_uploading = false; /*< Are we uploading a mission atm. Needed to know what messages/timeouts to react to*/
    int mission_retries = 0;
    mavlink_mission_item_int_t last_single_mission_upload;

    CppTime::Timer timer_control;
    CppTime::timer_id mission_ack_timeout;
    CppTime::timer_id mission_up_timeout;

    /// Command Protocol
    unsigned int confirmation = 0; // increments for each timeout of same command. Useful to monitor if a command should be killed
    CppTime::timer_id command_timeout; // timeout for commands.
    mavlink_command_long_t last_cmd_long; // Saving parameters for resending last command

    /// function prototypes
    void ml_send_mission_count();
    void ml_send_mission_clear_all();
    void ml_send_mission_item_int();
    void ml_send_single_mission_item_int(mavlink_mission_item_int_t last_single_mission_upload);
    void ml_send_command_long(unsigned short cmd_id, float p1, float p2, float p3, float p4, float p5, float p6, float p7);
    string command_result_parser(uint8_t result);
    string mission_result_parser(uint8_t result);

    /// command interfaces
    void command_arm_disarm(bool arm);
    void command_start_mission(int first_item, int last_item);
    void command_set_mode(float mode, float custom_mode, float custom_sub_mode);
    void command_land(float abort_alt, float precision_land_mode, float yaw_angle, float lat, float lon, float altitude);
    void command_do_reposition(float ground_speed, float yaw_heading, float lat, float lon, float alt);
    void command_do_pause_continue(const bool continue_mission);

    /// MQTT callbacks
    void ml_mission_clear_all_callback();
    void ml_send_heartbeat_callback(const mavlink_heartbeat_t &msg);
    void ml_new_mission_callback(const vector<mavlink_mission_item_int_t> & waypoints);

    /// Timers and their callbacks
    void mission_ack_timeout_callback();
    void mission_up_item_timeout_callback();
    void mission_up_count_timeout_callback();
    void mission_clear_all_timeout_callback();
    void mission_upload_single_item_timeout_callback();
    void command_long_timeout_callback();
    void ml_command_start_mission_callback(int first_item, int last_item);
};


#endif //MAVLINK_BRIDGE_MAVLINK_BRIDGE_HPP
