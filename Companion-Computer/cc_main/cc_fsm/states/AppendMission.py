#!/usr/bin/env python

import smach
import threading
from datetime import datetime
import time
from copy import deepcopy
from cc_fsm.CustomDefines import *
import cc_fsm.protocol.cc_messages_pb2 as messages


# define state
class AppendMission(smach.State):
    def __init__(self):
        # init smach state
        smach.State.__init__(self,
                             outcomes=['telemetry_control'],
                             input_keys=['missionList', 'mqtt_client', 'mission_append'],
                             output_keys=['missionList', 'mqtt_client'])

        # vars
        self.ud = None

        # workaround to change state from callback functions
        self.nextState = ""
        self.mutex = threading.Lock()

        self.item_receive_timer = None
        self.curr_mission_timer = None
        self.upload_timeout = 1.5  # max 2 sec, between items is assumed. We ask for next item if longer
        self.sent_mission_to_fc = False
        self.mission_count = 0
        self.mission_next_item = 0
        self.new_mission_list = []
        self.retries = 0
        self.current_mission = None

        # setup subs
        self.client = None

    def on_state_transition(self):
        # release mqtt callback?
        self.client.message_callback_remove("mission/single_item")
        self.client.message_callback_remove("mission/ack")
        self.client.message_callback_remove("mission/current")

        # release mutex and stop timers
        self.mutex.release()

        if self.item_receive_timer is not None:
            self.item_receive_timer.cancel()

        if self.curr_mission_timer is not None:
            self.curr_mission_timer.cancel()

    def execute(self, userdata):
        print("Executing State APPEND MISSION", flush=True)

        self.ud = userdata
        self.client = self.ud.mqtt_client

        # clean nextstate variable if we are revisiting the state
        self.nextState = ""
        self.sent_mission_to_fc = False
        self.current_mission = None

        # TODO cheating a bit. Using end_index as count
        self.mission_count = self.ud.mission_append.end_index
        self.mission_next_item = 0

        self.new_mission_list = []
        self.retries = 0

        # setup subscribers,
        # All Mavlink messages => passthrough
        self.client.message_callback_add("mission/upload/single_item", self.msg_mission_item_callback)
        self.client.message_callback_add("mission/ack", self.msg_mission_ack_callback)
        self.client.message_callback_add("mission/current", self.msg_mission_current_callback)

        # debug
        print("Mission length: {0}".format(self.mission_count), flush=True)

        # send request for next sequence
        self.sendItemRequest()

        while True:
            # next state check
            self.mutex.acquire()
            if self.nextState != "":
                self.on_state_transition()
                return self.nextState

            self.mutex.release()

            # sleep 100 ms. should mean we check with 10hz, which is more than enough
            time.sleep(0.1)

    def sendItemRequest(self):
        self.mutex.acquire()

        # todo put in max retries of 5. Only tries to send request 5 times, before it decides that communication is lost and abort the upload
        if self.retries >= 5:
            print("MAX RETRIES, ABORTING", flush=True)
            self.nextState = "telemetry_control"
            self.mutex.release()
            return

        # send request for next mission item
        item = messages.mission_request_int()
        item.seq = self.mission_next_item

        payload = bytearray(item.SerializeToString())
        self.client.publish("mission/upload/request_item", payload, qos=1)

        print("Mission request for item {0} sent".format(item.seq), flush=True)

        self.retries += 1

        # start timer for resend if we haven't received next item yet
        self.item_receive_timer = threading.Timer(self.upload_timeout, self.sendItemRequest)
        self.item_receive_timer.start()

        self.mutex.release()

    def msg_mission_item_callback(self, client, userdata, msg):
        self.mutex.acquire()

        # reset timer and retires
        self.item_receive_timer.cancel()
        self.retries = 0

        # parse msg
        item = messages.mission_item()
        item.ParseFromString(msg.payload)

        # if item we expected, save it and increment and ask for next item
        # todo Hack subtracting start_index. To make first item seq correspond to zero when asking GCS
        if item.seq == self.mission_next_item:
            self.new_mission_list.append(item)
            self.mission_next_item += 1
            print("Received and appended mission item with seq: {}".format(item.seq), flush=True)

        # release mutex before calling new function
        self.mutex.release()

        if self.mission_next_item == self.mission_count and not self.sent_mission_to_fc:
            # we got all items, upload to FC
            self.send_mission_FC()
        elif not self.sent_mission_to_fc:
            # send next request
            self.sendItemRequest()

    def msg_mission_ack_callback(self, client, userdata, msg):
        # todo this is broadcast down to groundstation too. Is final response telling that we finished and
        #  should change back to telemControl state
        # TODO Is also here we should finally save new mission into missionList

        # parse msg
        ack = messages.mission_ack()
        ack.ParseFromString(msg.payload)

        if self.sent_mission_to_fc:
            self.mutex.acquire()
            # todo react on failure besides just printing?
            print(ack.result_text)

            # if success
            if ack.result == 0:
                # overwrite current missionlist
                self.ud.missionList = deepcopy(self.new_mission_list)
                self.nextState = 'telemetry_control'
            else:
                print("Mission upload to FC Failed", flush=True)
                self.nextState = 'telemetry_control'

            self.mutex.release()

    def msg_mission_current_callback(self, client, userdata, msg):
        # parse msg
        curr = messages.mission_current()
        curr.ParseFromString(msg.payload)

        # save current
        self.mutex.acquire()
        self.current_mission = curr.seq
        self.mutex.release()

    def send_mission_FC(self):
        print("ALL MISSION ITEMS RECEIVED", flush=True)
        # we got full mission. Make it into right format and publish it. Then set
        #  flag that the next mission_ack is response to our full upload to FC
        self.mutex.acquire()

        # set flag so we don't get in here multiple times
        self.sent_mission_to_fc = True

        # check if current mission is not empty
        if self.current_mission is None:

            # check for max retries
            if self.retries >= 5:
                print("MAX RETRIES: Failed on getting current mission item", flush=True)
                self.nextState = "telemetry_control"
                self.mutex.release()
                return

            # wait 1sec and try again
            print("Not received current waypoint yet, trying again in 1sec...", flush=True)
            self.curr_mission_timer = threading.Timer(1.0, self.send_mission_FC)
            self.curr_mission_timer.start()

            self.retries += 1

            self.mutex.release()
            return

        msg = messages.mission_list()

        print("Preparing mission list", flush=True)
        print("Current mission: {0}, start_index: {1}".format(self.current_mission, self.ud.mission_append.start_index), flush=True)
        # make new list by copying current list starting from current mission index to start_index of append.
        # TODO note: This can replace existing items as it appends and overwrites from start_index and forwards
        temp_mission_list = self.ud.missionList[self.current_mission:self.ud.mission_append.start_index]
        temp_mission_list.extend(self.new_mission_list)

        # save in class variable for deepcopy on accept ack. Deepcopy to not change seq of current mission list
        self.new_mission_list = deepcopy(temp_mission_list)

        # sort out seq
        for idx, item in enumerate(self.new_mission_list):
            print("old seq: {}, new seq: {}".format(item.seq, idx), flush=True)
            item.seq = idx

        # make first mission item current
        self.new_mission_list[0].current = 1

        # make msg and send to mavlink_bridge
        msg.waypoints.extend(self.new_mission_list)

        self.mutex.release()

        # publish, using bytearray to insure no funny stuff with utf8 encoding and strings
        payload = bytearray(msg.SerializeToString())
        self.client.publish("mission/upload", payload, qos=2)